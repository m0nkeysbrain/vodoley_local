<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Схема");
?><?$APPLICATION->IncludeComponent("bitrix:news.index", "filter", Array(
	"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
		"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
		"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
		"FIELD_CODE" => array(	// Поля
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
		"IBLOCKS" => array(	// Код информационного блока
			0 => "7",
			1 => "8",
			2 => "9",
			3 => "10",
		),
		"IBLOCK_SORT_BY" => "SORT",	// Поле для сортировки информационных блоков
		"IBLOCK_SORT_ORDER" => "ASC",	// Направление для сортировки информационных блоков
		"IBLOCK_TYPE" => "Areas",	// Тип информационных блоков
		"IBLOCK_URL" => "",	// URL, ведущий на страницу с содержимым раздела
		"NEWS_COUNT" => "100",	// Количество новостей в каждом блоке
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
		),
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>