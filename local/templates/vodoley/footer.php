<section class="contact">
    <div class="map" id="map"></div>
    <div class="contact-info">
        <h2 class="zigzag">наши контакты</h2>
        <div class="adress">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR."local/include/adress.php"
                )
            );?>    
        </div>
        <div class="tel">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR."local/include/phone.php"
                )
            );?>                
        </div>
        <div class="email">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR."local/include/mail.php"
                )
            );?>     </div>
        <div class="time-work">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR."local/include/time.php"
                )
            );?>       
        </div>
        <div class="metro">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR."local/include/metro.php"
                )
            );?>
        </div>
        <hr>
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR."local/include/parking.php"
                )
            );?>
    </div>
</section>

<?$APPLICATION->IncludeComponent(
	"bitrix:news", 
	"partners", 
	array(
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "3",
		"IBLOCK_TYPE" => "News",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"LIST_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "IMG",
			2 => "",
		),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SEF_MODE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "partners",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"VARIABLE_ALIASES" => array(
			"SECTION_ID" => "SECTION_ID",
			"ELEMENT_ID" => "ELEMENT_ID",
		)
	),
	false
);?><br>
<?$APPLICATION->IncludeComponent(
    "slam:easyform",
    "send",
    Array(
        "CAPTCHA_ERROR" => "Обязательное поле",
        "CAPTCHA_KEY" => "",
        "CAPTCHA_SECRET_KEY" => "",
        "CAPTCHA_TITLE" => "",
        "CATEGORY_EMAIL_PLACEHOLDER" => "Введите E-mail",
        "CATEGORY_EMAIL_TITLE" => "",
        "CATEGORY_EMAIL_TYPE" => "email",
        "CATEGORY_EMAIL_VALIDATION_ADDITIONALLY_MESSAGE" => "data-bv-emailaddress-message=\"E-mail введен некорректно\"",
        "CATEGORY_EMAIL_VALIDATION_MESSAGE" => "Обязательное поле",
        "CATEGORY_EMAIL_VALUE" => "",
        "CATEGORY_MESSAGE_PLACEHOLDER" => "",
        "CATEGORY_MESSAGE_TITLE" => "Сообщение",
        "CATEGORY_MESSAGE_TYPE" => "textarea",
        "CATEGORY_MESSAGE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
        "CATEGORY_MESSAGE_VALUE" => "",
        "CATEGORY_PHONE_INPUTMASK" => "N",
        "CATEGORY_PHONE_INPUTMASK_TEMP" => "+7 (999) 999-9999",
        "CATEGORY_PHONE_PLACEHOLDER" => "",
        "CATEGORY_PHONE_TITLE" => "Мобильный телефон",
        "CATEGORY_PHONE_TYPE" => "tel",
        "CATEGORY_PHONE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
        "CATEGORY_PHONE_VALUE" => "",
        "CATEGORY_TITLE_PLACEHOLDER" => "",
        "CATEGORY_TITLE_TITLE" => "Ваше имя",
        "CATEGORY_TITLE_TYPE" => "text",
        "CATEGORY_TITLE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
        "CATEGORY_TITLE_VALUE" => "",
        "CREATE_SEND_MAIL" => "",
        "DISPLAY_FIELDS" => array("EMAIL",""),
        "EMAIL_BCC" => "",
        "EMAIL_TO" => "monkeysbrain@yandex.ru",
        "ENABLE_SEND_MAIL" => "Y",
        "ERROR_TEXT" => "Произошла ошибка. Подписка не удалась",
        "EVENT_MESSAGE_ID" => array(),
        "FIELDS_ORDER" => "EMAIL",
        "FORM_AUTOCOMPLETE" => "Y",
        "FORM_ID" => "FORM3",
        "FORM_NAME" => "Форма обратной связи 2",
        "FORM_SUBMIT_VALUE" => "",
        "FORM_SUBMIT_VARNING" => "Нажимая на кнопку \"#BUTTON#\", вы даете согласие на обработку <a target=\"_blank\" href=\"#\">персональных данных</a>",
        "HIDE_ASTERISK" => "N",
        "HIDE_FIELD_NAME" => "N",
        "HIDE_FORMVALIDATION_TEXT" => "N",
        "INCLUDE_BOOTSRAP_JS" => "Y",
        "MAIL_SUBJECT_ADMIN" => "#SITE_NAME#: Сообщение из формы обратной связи",
        "OK_TEXT" => "Вы подписаны!",
        "REPLACE_FIELD_FROM" => "N",
        "REQUIRED_FIELDS" => array("EMAIL"),
        "SEND_AJAX" => "Y",
        "SHOW_MODAL" => "N",
        "TITLE_SHOW_MODAL" => "Спасибо!",
        "USE_BOOTSRAP_CSS" => "N",
        "USE_BOOTSRAP_JS" => "N",
        "USE_CAPTCHA" => "N",
        "USE_FORMVALIDATION_JS" => "Y",
        "USE_IBLOCK_WRITE" => "N",
        "USE_JQUERY" => "N",
        "USE_MODULE_VARNING" => "Y",
        "WIDTH_FORM" => "500px",
        "WRITE_MESS_FILDES_TABLE" => "N",
        "_CALLBACKS" => ""
    )
);?><br>
    <footer class="footer container">
        <hr>
        <div class="footer-top">
		    <?$APPLICATION->IncludeComponent(
		        "bitrix:main.include",
		        "",
		        Array(
		            "AREA_FILE_SHOW" => "file",
		            "PATH" => SITE_DIR."local/include/logo.php"
		        )
		    );?>	
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"bottom",
				Array(
					"ALLOW_MULTI_SELECT" => "N",
					"CHILD_MENU_TYPE" => "left",
					"DELAY" => "N",
					"MAX_LEVEL" => "1",
					"MENU_CACHE_GET_VARS" => array(0=>"",),
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_TYPE" => "N",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"ROOT_MENU_TYPE" => "left",
					"USE_EXT" => "N"
				)
			);?>
        </div>
        <div class="footer-bottom">
            <div class="metro">
			    <?$APPLICATION->IncludeComponent(
			        "bitrix:main.include",
			        "",
			        Array(
			            "AREA_FILE_SHOW" => "file",
			            "PATH" => SITE_DIR."local/include/metro-footer.php"
			        )
			    );?>
            </div>
            <div class="creator">
			    <?$APPLICATION->IncludeComponent(
			        "bitrix:main.include",
			        "",
			        Array(
			            "AREA_FILE_SHOW" => "file",
			            "PATH" => SITE_DIR."local/include/create.php"
			        )
			    );?>			    	
			</div>
        </div>
        <div class="copirated">
		    <?$APPLICATION->IncludeComponent(
		        "bitrix:main.include",
		        "",
		        Array(
		            "AREA_FILE_SHOW" => "file",
		            "PATH" => SITE_DIR."local/include/copy.php"
		        )
		    );?></div>
    </footer>

    <div class="hidden_content" hidden>
        <svg viewBox="0 0 35.47 57">
            <path id="ballun-body" d="m 456.90023,251.76453 c -0.2,-9.8 -8.2,-17.6 -18,-17.5 -9.7,0.1 -17.6,8 -17.5,17.7 0,0.1 0,0.2 0,0.2 0.1,7.2 4.4,13.3 10.6,16 v 0 c 2,1.8 4.9,6.8 5.7,20.5 0.4,0.9 1.3,2.5 1.3,2.5 0,0 0.8,-1.6 1.3,-2.5 0.8,-13 3.4,-18.1 5.3,-20.2 6.6,-2.5 11.4,-9.1 11.3,-16.7 z"
            style="fill:#1e3e56;"></path>
            <path class="fill-white" id="fill-white" d="m 432.80023,248.96453 c 0,-0.8 0.2,-1.4 0.6,-2 0.4,-0.5 0.9,-0.8 1.7,-0.8 0.7,0 1.2,0.2 1.7,0.7 0.4,0.5 0.7,1.2 0.7,2.1 0,0.9 -0.2,1.6 -0.7,2.1 -0.4,0.5 -1,0.7 -1.7,0.7 -0.6,0 -1.2,-0.2 -1.6,-0.7 -0.5,-0.4 -0.7,-1.1 -0.7,-2.1 z m 2.3,-1.8 c -0.3,0 -0.6,0.1 -0.8,0.4 -0.2,0.3 -0.3,0.8 -0.3,1.6 0,0.7 0.1,1.2 0.3,1.5 0.2,0.3 0.5,0.4 0.8,0.4 0.3,0 0.6,-0.1 0.8,-0.4 0.2,-0.3 0.3,-0.8 0.3,-1.6 0,-0.7 -0.1,-1.2 -0.3,-1.5 -0.2,-0.3 -0.5,-0.4 -0.8,-0.4 z m 0,10.4 5.8,-11.3 h 1.1 l -5.9,11.3 z m 4.6,-2.9 c 0,-0.8 0.2,-1.4 0.6,-2 0.4,-0.5 0.9,-0.8 1.7,-0.8 0.7,0 1.2,0.2 1.7,0.7 0.4,0.5 0.7,1.2 0.7,2.1 0,0.9 -0.2,1.6 -0.7,2.1 -0.4,0.5 -1,0.7 -1.7,0.7 -0.6,0 -1.2,-0.2 -1.6,-0.7 -0.5,-0.4 -0.7,-1.2 -0.7,-2.1 z m 2.3,-1.9 c -0.3,0 -0.6,0.1 -0.8,0.4 -0.2,0.3 -0.3,0.8 -0.3,1.6 0,0.7 0.1,1.2 0.3,1.5 0.2,0.3 0.5,0.4 0.8,0.4 0.3,0 0.6,-0.1 0.8,-0.4 0.2,-0.3 0.3,-0.8 0.3,-1.6 0,-0.7 -0.1,-1.2 -0.3,-1.5 -0.2,-0.2 -0.5,-0.4 -0.8,-0.4 z"></path>
            <g id="ballun">
                <path d="m 456.90023,251.76453 c -0.2,-9.8 -8.2,-17.6 -18,-17.5 -9.7,0.1 -17.6,8 -17.5,17.7 0,0.1 0,0.2 0,0.2 0.1,7.2 4.4,13.3 10.6,16 v 0 c 2,1.8 4.9,6.8 5.7,20.5 0.4,0.9 1.3,2.5 1.3,2.5 0,0 0.8,-1.6 1.3,-2.5 0.8,-13 3.4,-18.1 5.3,-20.2 6.6,-2.5 11.4,-9.1 11.3,-16.7 z"
                style="fill:#b23803;"></path>
                <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#0e1f31;"></circle>
            </g>
            <g id="elevator" transform="translate(490.77243,288.14024)" style="fill:#ffffff;">
                <circle cx="-47.563499" cy="-42.313049" r="1.4" style="fill:#ffffff;"></circle>
                <path d="m -45.963499,-40.513048 h -0.8 l -0.7,1.4 -0.7,-1.4 h -0.7 c -0.4,0 -0.8,0.6 -0.8,0.9 v 10.3 c 0,0.4 0.3,0.7 0.7,0.7 0.4,0 0.7,-0.3 0.7,-0.7 v -4.4 h 1.4 v 4.4 c 0,0.4 0.3,0.7 0.7,0.7 0.4,0 0.7,-0.3 0.7,-0.7 v -10.2 c 0.1,-0.4 -0.1,-1 -0.5,-1 z"
                style="fill:#ffffff;"></path>
                <path d="m -47.463499,-45.313048 h -8.4 c -2.8,0 -5,2.2 -5,5 v 8.4 c 0,2.8 2.2,5 5,5 h 8.4 c 2.8,0 5,-2.2 5,-5 v -8.4 c 0,-2.8 -2.2,-5 -5,-5 z m -0.7,17.7 h -7 c -2.8,0 -5,-2.2 -5,-5 v -7 c 0,-2.8 2.2,-5 5,-5 h 7 c 2.8,0 5,2.2 5,5 v 7 c 0,2.8 -2.2,5 -5,5 z"></path>
                <path d="m -58.363499,-36.313048 c 0.3,0.2 0.8,0.1 1,-0.2 l 0.5,-0.6 v 6.3 c 0,0.4 0.3,0.7 0.7,0.7 0.4,0 0.7,-0.3 0.7,-0.7 v -10.6 l -3.1,4.1 c -0.2,0.3 -0.1,0.7 0.2,1 z"></path>
                <path d="m -51.763499,-35.113048 c -0.3,-0.2 -0.8,-0.1 -1,0.2 l -0.5,0.6 v -6.3 c 0,-0.4 -0.3,-0.7 -0.7,-0.7 -0.4,0 -0.7,0.3 -0.7,0.7 v 10.6 l 3.1,-4.1 c 0.2,-0.3 0.1,-0.8 -0.2,-1 z"></path>
            </g>
            <g id="toilet" transform="translate(495.04466,243.97185)" style="fill:#ffffff;">
                <circle cx="-61.601891" cy="1.7307615" r="1.4" style="fill:#ffffff;"></circle>
                <path d="m -60.101889,3.8307616 h -0.8 l -0.7,1.4 -0.7,-1.4 h -0.6 c -0.3,0 -0.6,0.3 -0.6,0.7 v 4.8 1.4000004 4.1 c 0,0.4 0.3,0.7 0.7,0.7 0.4,0 0.7,-0.3 0.7,-0.7 v -4.1 h 1.4 v 4.1 c 0,0.4 0.3,0.7 0.7,0.7 0.4,0 0.7,-0.3 0.7,-0.7 v -4.1 -1.4000004 -4.8 c -0.1,-0.4 -0.4,-0.7 -0.8,-0.7 z"
                style="fill:#ffffff;"></path>
                <path d="m -56.001889,-1.6692384 c -0.4,0 -0.7,0.3 -0.7,0.69999996 V 15.430762 c 0,0.4 0.3,0.7 0.7,0.7 0.4,0 0.7,-0.3 0.7,-0.7 V -0.96923844 c 0,-0.39999996 -0.3,-0.69999996 -0.7,-0.69999996 z"></path>
                <path d="m -50.501889,3.1307616 c 0.4,0 0.8,-0.2 1.1,-0.5 l 1,0.5 -0.7,-1.4 c 0,-0.80000004 -0.6,-1.40000004 -1.4,-1.40000004 -0.8,0 -1.4,0.6 -1.4,1.40000004 l -0.7,1.4 1,-0.5 c 0.3,0.3 0.7,0.5 1.1,0.5 z"></path>
                <path d="m -49.101889,8.6307616 0.7,-4.1 c 0,-0.4 -0.3,-0.7 -0.7,-0.7 h -0.7 c 0,0 0,0.7 -0.7,0.7 -0.7,0 -0.7,-0.7 -0.7,-0.7 h -0.7 c -0.4,0 -0.7,0.3 -0.7,0.7 l 0.7,4.1 -2.2,4.1000004 h 1.5 v 2.1 c 0,0.4 0.3,0.7 0.7,0.7 0.4,0 0.7,-0.3 0.7,-0.7 v -2.1 h 1.4 v 2.1 c 0,0.4 0.3,0.7 0.7,0.7 0.4,0 0.7,-0.3 0.7,-0.7 v -2.1 h 1.3 z"></path>
            </g>
            <path id="key" d="m 442.25155,243.87769 c -2.5,0 -4.6,2 -4.6,4.6 0,0.6 0.1,1.1 0.3,1.6 l -5.6,5.6 v 2.7 h 2.7 v -1.6 h 1.6 v -1.6 h 1.6 l 2.4,-2.4 c 0.5,0.2 1,0.3 1.6,0.3 2.5,0 4.6,-2 4.6,-4.6 -0.1,-2.6 -2.1,-4.6 -4.6,-4.6 z m 1.3,4.8 c -0.9,0 -1.6,-0.7 -1.6,-1.6 0,-0.9 0.7,-1.6 1.6,-1.6 0.9,0 1.6,0.7 1.6,1.6 0,0.9 -0.7,1.6 -1.6,1.6 z"
            style="fill:#ffffff;"></path>
            <g id="exit">
                <path d="m 438.29323,248.5946 a 1.2,1.2 0 1 0 -1.2,-1.2 1.2,1.2 0 0 0 1.2,1.2 z" style="fill:#ffffff;"></path>
                <path d="m 441.28323,250.1846 a 0.71,0.71 0 0 0 -0.54,0.08 c -0.32,0.2 -1,1.61 -2.07,-0.65 a 1.68,1.68 0 0 0 -0.87,-0.85 4.56,4.56 0 0 0 -0.93,-0.28 3.27,3.27 0 0 0 -2.31,0.41 4.05,4.05 0 0 0 -1.92,2.23 0.74,0.74 0 0 0 0,0.44 c 0.14,0.34 0.39,0.26 0.64,0.17 a 0.91,0.91 0 0 0 0.36,-0.48 2.1,2.1 0 0 1 1.75,-1.51 c 0,0 0.72,-0.1 0.33,0.54 -0.39,0.64 -1.59,2.13 -1.46,3.63 a 2,2 0 0 1 -0.77,1.92 5.15,5.15 0 0 1 -1.85,0.8 0.61,0.61 0 0 0 -0.56,0.59 v 0 a 0.53,0.53 0 0 0 0.59,0.59 h 0.45 a 4.87,4.87 0 0 0 1.8,-0.77 3.93,3.93 0 0 0 1.84,-2.14 c 0.09,-0.38 0.27,-0.71 0.66,-0.7 a 1.21,1.21 0 0 1 0.36,0.07 3.85,3.85 0 0 1 1.5,0.88 5.48,5.48 0 0 1 0.75,2.77 0.63,0.63 0 0 0 0.46,0.6 0.55,0.55 0 0 0 0.69,-0.44 3.15,3.15 0 0 0 0,-1.06 c -0.17,-1.05 -0.62,-3 -1.78,-3.56 -1.61,-0.85 -1.48,-1.19 -0.5,-3 0,0 1.1,2.26 3,0.81 a 0.83,0.83 0 0 0 0.39,-0.82 0.49,0.49 0 0 0 -0.01,-0.27 z"
                style="fill:#ffffff;"></path>
                <polygon points="22.23,17.21 22.23,15.71 24.26,17.8 22.18,19.91 22.15,18.44 20.36,18.39 20.36,23.05 25.12,25.85 25.12,9.55 20.36,12.07 20.36,17.21 " transform="translate(421.64323,234.1246)" style="fill:#ffffff;"></polygon>
            </g>
            <g id="bank">
                <circle cx="437.0665" cy="244.68336" r="1.73" style="fill:#ffffff;"></circle>
                <path d="m 440.49648,243.81336 v 3.73 l -0.61,0.31 v 2.55 c 0,0 -2.86,1.52 -2.82,-1.38 0.04,-2.9 -2.08,-2.87 -2.08,-2.87 -2.08,0.4 -2.6,6.07 -2.6,6.07 l 0.27,0.78 v 6.81 a 0.57,0.57 0 0 0 0.25,0.48 2,2 0 0 0 1.89,0.13 0.86,0.86 0 0 0 0.54,-0.8 v -6.53 a 5.17,5.17 0 0 0 0.69,-0.26 l 0.13,-2.12 c 1.73,2.21 4.29,0.62 4.29,0.62 l 0.05,9.27 h 4.89 v -16.79 z m 3.41,5.48 a 1.24,1.24 0 0 1 -0.82,0.43 v 0.5 h -0.55 v -0.5 a 1.27,1.27 0 0 1 -0.78,-0.44 1.48,1.48 0 0 1 -0.3,-0.91 h 0.7 a 0.72,0.72 0 0 0 0.18,0.53 0.67,0.67 0 0 0 0.5,0.18 0.7,0.7 0 0 0 0.5,-0.17 0.66,0.66 0 0 0 0.17,-0.47 0.63,0.63 0 0 0 -0.11,-0.35 1.09,1.09 0 0 0 -0.28,-0.29 c -0.12,-0.08 -0.24,-0.17 -0.38,-0.25 a 4.28,4.28 0 0 1 -0.42,-0.26 4.08,4.08 0 0 1 -0.39,-0.31 1.3,1.3 0 0 1 -0.28,-0.4 1.27,1.27 0 0 1 -0.11,-0.49 1.15,1.15 0 0 1 0.27,-0.77 1.19,1.19 0 0 1 0.73,-0.39 v -0.54 h 0.55 v 0.5 a 1.13,1.13 0 0 1 0.76,0.42 1.45,1.45 0 0 1 0.29,0.88 h -0.69 a 0.73,0.73 0 0 0 -0.17,-0.5 0.59,0.59 0 0 0 -0.44,-0.17 0.6,0.6 0 0 0 -0.44,0.15 0.51,0.51 0 0 0 -0.16,0.4 0.59,0.59 0 0 0 0.2,0.45 2.33,2.33 0 0 0 0.49,0.37 c 0.19,0.11 0.39,0.22 0.58,0.35 a 1.56,1.56 0 0 1 0.49,0.5 1.21,1.21 0 0 1 0.21,0.71 1.27,1.27 0 0 1 -0.3,0.84 z"
                style="fill:#ffffff;"></path>
            </g>
            <g id="escalator">
                <circle cx="438.92508" cy="244.88222" r="1.27" style="fill:#ffffff;"></circle>
                <path d="m 440.43509,248.93222 c 0,0 0,-2 -1.48,-2.06 -1.48,-0.06 -2.21,2.06 -1.47,5.45 z" style="fill:#ffffff;"></path>
                <path d="m 447.97509,247.20222 v 0 a 0.59,0.59 0 0 0 -0.52,-0.33 h -4 a 0.78,0.78 0 0 0 -0.61,0.28 l -7.92,9.68 a 0.88,0.88 0 0 1 -0.69,0.33 h -2.49 a 1,1 0 0 0 -0.88,0.55 v 0 a 2,2 0 0 0 0,1.83 1,1 0 0 0 0.85,0.52 h 3.48 a 1.2,1.2 0 0 0 0.94,-0.45 l 7.68,-9.65 a 0.73,0.73 0 0 1 0.57,-0.29 l 3.12,-0.07 a 0.5,0.5 0 0 0 0.44,-0.28 v 0 a 2.44,2.44 0 0 0 0.03,-2.12 z"
                style="fill:#ffffff;"></path>
            </g>
            <g id="terminal">
                <path d="m 444.77869,245.39063 v -2 h -8.28 l -3.11,7.21 v 2 h 4.73 l 0.47,4.73 c 0,0 0.17,1.13 -0.31,1.63 a 10.23,10.23 0 0 1 -1.58,1.23 0.38,0.38 0 0 0 -0.18,0.33 v 0.75 a 0.25,0.25 0 0 0 0.26,0.26 h 3.86 a 0.51,0.51 0 0 0 0.36,-0.15 l 1,-1.08 a 1.41,1.41 0 0 0 0.4,-0.87 4.61,4.61 0 0 0 -0.08,-1.34 l -0.53,-5.45 h 0.56 z m -3.53,5 h -6.71 l 2.71,-6.26 h 6.45 z"
                style="fill:#ffffff;"></path>
                <polygon points="14.35,15.57 18.74,15.57 20.66,11.24 16.08,11.24 " transform="translate(421.69869,233.77063)" style="fill:#ffffff;"></polygon>
            </g>
        </svg>
    </div>


    <!--<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/main5km5d8ic3rj4b5bl0udi.min.js"></script>-->
    <script src="<?=SITE_TEMPLATE_PATH?>/distr/js/d3.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/distr/js/jquery.dotdotdot.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/distr/js/jquery.fancybox.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/distr/js/js.cookie.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/distr/js/slick.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/distr/js/swiper.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/distr/js/init.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</body>
</html>