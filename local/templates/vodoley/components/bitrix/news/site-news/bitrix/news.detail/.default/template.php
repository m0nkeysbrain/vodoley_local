<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
	// сортировку берем из параметров компонента
	$arSort = array(
	      $arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
	      $arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	   );
	// выбрать нужно id элемента, его имя и ссылку. Можно добавить любые другие поля, например PREVIEW_PICTURE или PREVIEW_TEXT
	$arSelect = array(
	      "ID",
	      "NAME",
	      "DETAIL_PAGE_URL"
	   );
	// выбираем активные элементы из нужного инфоблока. Раскомментировав строку можно ограничить секцией
	$arFilter = array (
	      "IBLOCK_ID" => $arResult["IBLOCK_ID"],
	      //"SECTION_CODE" => $arParams["SECTION_CODE"],
	      "ACTIVE" => "Y",
	      "CHECK_PERMISSIONS" => "Y",
	   );
	// выбирать будем по 1 соседу с каждой стороны от текущего
	$arNavParams = array(
	      "nPageSize" => 1,
	      "nElementID" => $arResult["ID"],
	   );
	$arItems = Array();
	$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams, $arSelect);
	$rsElement->SetUrlTemplates($arParams["DETAIL_URL"]);
	while($obElement = $rsElement->GetNextElement())
	      $arItems[] = $obElement->GetFields();
	// возвращается от 1го до 3х элементов в зависимости от наличия соседей, обрабатываем эту ситуацию      
	if(count($arItems)==3):
	   $arResult["TORIGHT"] = Array("NAME"=>$arItems[0]["NAME"], "URL"=>$arItems[0]["DETAIL_PAGE_URL"]);
	   $arResult["TOLEFT"] = Array("NAME"=>$arItems[2]["NAME"], "URL"=>$arItems[2]["DETAIL_PAGE_URL"]);
	elseif(count($arItems)==2):
	   if($arItems[0]["ID"]!=$arResult["ID"])
	      $arResult["TORIGHT"] = Array("NAME"=>$arItems[0]["NAME"], "URL"=>$arItems[0]["DETAIL_PAGE_URL"]);
	   else
	      $arResult["TOLEFT"] = Array("NAME"=>$arItems[1]["NAME"], "URL"=>$arItems[1]["DETAIL_PAGE_URL"]);
	endif;
	// в $arResult["TORIGHT"] и $arResult["TOLEFT"] лежат массивы с информацией о соседних элементах
?>
        <section class="single-action-page container">
            <h2 class="zigzag"><?=$arResult['NAME']?></h2>
            <div class="single-action-page_body">
                <div class="img-wrap"><span class="image-title"><?=$arResult['NAME']?></span><!--TODO set if-->
                    <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult['DETAIL_PICTURE']['ALT']?>">
                </div>
                <?=$arResult['~DETAIL_TEXT']?>
                </p>
                <div class="btn-row">
                    <div class="nav-btn">
						<?if(is_array($arResult["TOLEFT"])):?> 
						   <a class="prev-page" href="<?=$arResult["TOLEFT"]["URL"]?>"></a> 
						<?endif?>
						<?if(is_array($arResult["TORIGHT"])):?> 
						   <a class="next-page" href="<?=$arResult["TORIGHT"]["URL"]?>"></a> 
						<?endif?>
                    </div><a class="button button__social">Поделиться</a>
                </div>
			<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
			<script src="//yastatic.net/share2/share.js"></script>
			<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus,twitter,tumblr,viber,whatsapp,telegram"></div>
            </div>
            <div class="single-action-page_aside">
                <h2>BERSHKA</h2>
                <p>Элитный магазин женской одежды и обуви для ценителей моды</p>
                <div class="detalied-row">
                    <ul>
                        <li>Секция 1, этаж 1</li>
                        <li>Bershka</li>
                        <li>Adidas</li>
                        <li>Pull And Bear</li>
                        <li>Fishka</li>
                        <li>10:00-22:00</li>
                    </ul>
                </div>
                <div class="detalied-row">
                    <div class="detalied_col"><a href="tel:+78029285615">+7 (802) 928-56-15</a><a class="link" href="http://bershka.ru">bershka.ru</a>
                    </div>
                </div>
            </div>
        </section>