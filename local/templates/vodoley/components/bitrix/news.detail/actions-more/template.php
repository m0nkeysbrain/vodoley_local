<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>

<div class="single-action-page_aside">
    <h2><?=$arResult['PROPERTIES']['MARKET']['VALUE']?></h2>
    <p><?=$arResult['PREVIEW_TEXT']?></p>
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."local/include/detailed-row.php"
        )
    );?>  
    <div class="detalied-row">
        <div class="detalied_col">
        	<?=$arResult['PROPERTIES']['CONTACTS']['~VALUE']['TEXT']?>
        </div>
        <div class="detalied_col flex-container"><a class="button acsent-btn" href="/" data-area="BERSHKA">на схеме</a>
        </div>
    </div>
</div>


<script>
    $(function(){
        Cookies.remove('floor');
        Cookies.remove('elementh');
        var floor = "<?=$arResult['IBLOCK']['ID']?>";
        var elementh = "<?=$arResult['NAME']?>";
        Cookies.set('floor', floor);
        Cookies.set('elementh', elementh);
    })
</script>

    <!--<pre>
        <? print_r($arResult['NAME'])?>
    </pre>-->