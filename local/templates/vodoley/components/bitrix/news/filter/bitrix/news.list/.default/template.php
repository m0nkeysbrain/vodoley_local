<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list area-list">






<? $b==1 ?>
<?foreach($arResult["ITEMS"] as $arItem):?>
<?foreach($arItem['PROPERTIES']['CATEGORY']['VALUE_XML_ID'] as $xmlItem):?>
	<p><?=$xmlItem?></p>
<?endforeach;?>		
	<? $b++ ?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="area<?=$b?>">
		<p class="name"><?=$arItem['NAME']?></p>
		<p class="xml<?=$b?>">
			<?foreach($arItem['PROPERTIES']['CATEGORY']['VALUE_XML_ID'] as $xmlItem):?>
				<?=$xmlItem?>
			<?endforeach;?>
		</p>
	</div>
	<script>
		$(function(){
			var nameEl 	= $('.area<?=$b?> .name').html();
				xmlEl	= $('.xml<?=$b?>').html();
			 $('[data-area-target='+nameEl+']').addClass(xmlEl);
			 console.log($('[data-area-target='+nameEl+']').attr('class'))
		});
	</script>
<?endforeach;?>



	
</div>
