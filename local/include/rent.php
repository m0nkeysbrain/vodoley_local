<div class="container">
    <div class="shema-wrapper">
        <div class="thumbl-level">
            <div id="8" class="floor level-3"><span>3</span>
                <svg viewBox="0 0 233 129">
                    <path class="floor-3_path" d="M 3.1433688,49.657037 0.15216137,51.449357 144.7274,129.18503 232.5009,80.440372 v -3.468545 l -16.80077,-8.454578 11.38116,-6.503521 1e-5,-3.685329 -19.51057,-9.972067 -14.30775,8.02101 -62.97576,-33.601528 11.38116,-6.39513 L 141.45159,12.37018 121.39907,2.5065051 107.63328,10.419123 87.147189,-0.63686366 2.9265847,46.405276 Z"></path>
                </svg>
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/lvl-3-min.png" alt="">
                <h4>3 этаж</h4>
            </div>
            <div id="10" class="floor level-2"><span>2</span>
                <svg viewBox="0 0 231 127">
                    <path class="floor-2_path" d="m -0.10746162,53.603712 -1e-8,3.868618 130.78079163,69.63513 16.87148,-9.45662 10.20885,-0.42985 31.48626,-17.623704 -0.21493,-3.008925 -2.47162,-1.28954 1.18209,-0.752231 5.37308,2.794002 37.61156,-20.417708 -0.10746,-3.223849 -16.44163,-8.489468 5.91039,-3.33131 -0.42984,-3.438772 -18.37594,-9.779008 -8.38201,4.835773 -60.28597,-31.701178 5.91039,-3.761158 -0.32238,-3.33131 L 118.74509,5.0310581 110.14816,9.9742928 91.664764,-0.44948465 53.945735,19.968224 l -0.107462,3.223848 2.471617,1.611924 -1.611924,0.859694 -4.943234,-2.794002 -32.775795,17.408782 0.05373,4.943235 z"></path>
                </svg>
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/lvl-2-min.png" alt="">
                <h4>2 этаж</h4>
            </div>
            <div id="7" class="floor level-1 active"><span>1</span>
                <svg viewBox="0 0 229 127">
                    <path class="floor-1_path" d="M 80.644133,125.61509 106.10509,111.44644 134.1228,127.21306 228.93559,75.43889 228.82906,71.816828 93.747473,-0.51786932 0.31959366,51.469366 0.21306244,54.984896 25.247899,68.620892 2.7698117,80.978513 2.8763429,84.600575 29.935273,98.236571 c -0.205006,3.700839 2.727674,4.891539 8.202903,4.048189 l 7.670248,4.58084 c 0.156579,5.71462 4.770692,4.42455 8.73556,4.15472 z"></path>
                </svg>
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/lvl-1-min.png" alt="">
                <h4>1 этаж</h4>
            </div>
            <div id="9" class="floor level-0"><span>0</span>
                <svg viewBox="0 0 230 127">
                    <path class="floor-0_path" d="m 80.621804,126.57202 26.214126,-14.76551 28.83553,15.40748 94.90583,-51.839763 0.16049,-4.440352 -13.00006,-6.366287 c 3.49522,-1.854604 6.40195,-3.869704 9.89717,-5.724308 l 0.32098,-4.279857 -20.8108,-10.378653 -13.53505,8.185227 -58.63403,-31.40345 10.32515,-6.312789 V 11.871851 L 124.75783,0.31623763 110.6343,8.5014639 95.06632,-0.43273731 0.42798568,51.460527 0.4814839,55.686885 24.555678,69.061438 2.3539213,81.25903 v 3.691376 l 6.8477709,3.584381 4.1193628,-2.086431 61.362447,32.205924 -1.123462,0.96297 -0.160495,2.78191 z"></path>
                </svg>
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/lvl-0-min.png" alt="">
                <h4>0 этаж</h4>
            </div>
        </div>
        <div class="main-levels">
            <div class="main-level main-level-3">
                <svg viewBox="0 0 1000 553">
                    <path data-area-target="3-1" class="level-3_path-1 disable" d="M498.915,456.6l43.62-24.1,18.88,10.2-43.4,23.883Z"></path>
                    <g class="outer-ballun elevator-ballun" transform="translate(91.775295,158.41726)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#elevator"></use>
                    </g>
                    <path data-area-target="3-2" class="level-3_path-2 disable" d="M518.88,467.238l43.4-23.883,35.59,18.89L727.431,391.03l-72.266-38.864,55.555-30.614,42.752,22.8,23-.651L864.8,295.281l-4.991-15.2-14.757-7.816,9.332-5.428-34.071-18.455,70.1-38.43,81.814,43.641-60.113,33.219,85.938,46.029L638.021,530.637Z"></path>
                    <g class="outer-ballun" transform="translate(335.29541,120.35605)">

                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="3-3" class="level-3_path-3" d="M541.016,394.938l55.989,29.745-10.2,5.862,33.854,18.021-22.787,12.593-54.687-29.311Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/teremok.jpg"></path>
                    <g class="sale-ballun" transform="translate(129.93319,141.312)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="3-4" class="level-3_path-4" d="M541.233,393.852l47.743-25.837,90.494,48.2-57.942,31.916-32.986-17.587,10.2-5.862Z"></path>
                    <g class="sale-ballun" transform="translate(172.03845,119.60147)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="3-5" class="level-3_path-5" d="M589.627,367.581L634.983,342.4l90.711,48.418-45.355,24.968Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/kfc.png" data-area-target="KFS"></path>
                    <g class="sale-ballun" transform="translate(220.06477,94.60147)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="3-6" class="level-3_path-6" d="M753.906,343.7l22.353-.651,87.456-48.2-5.208-14.329-14.974-8.251,9.332-5.428-34.506-18.455,10.634-5.862-141.71-75.557L532.552,252.291,641.927,310.7l24.74-13.678Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/oldi.png"
                    data-area-target="OLDI"></path>
                    <g class="sale-ballun" transform="translate(265.55857,-32.933422)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="3-7" class="level-3_path-7" d="M471.354,340.658L500,325.026l29.08,15.632-28.646,15.85Z"></path>
                    <g class="sale-ballun" transform="translate(61.512137,51.180418)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="3-8" class="level-3_path-8" d="M411.458,308.959l28.429-16.067,29.08,15.633-28.646,15.85Z"></path>
                    <g class="sale-ballun" transform="translate(0.98582115,19.601471)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="3-9" class="level-3_path-9" d="M142.795,218.421l51.649,27.791,48.829,6.079,16.71-2.822,83.333-45.812-88.108-47.115Z"></path>
                    <g class="sale-ballun" transform="translate(-191.7821,-72.214051)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="3-10" class="level-3_path-10 disable" d="M180.99,239.916L137.37,263.8,16.059,198.88,376.085,1.086,462.24,47.115,522.786,13.9l35.157,18.889L473.524,79.031,450.955,67.09l-26.91,14.764-42.752-22.8-106.119,58.4,97.439,52.108L327.691,194.1l-72.483-38.647L141.059,218.638Z"></path>
                    <g class="outer-ballun" transform="translate(-199.67684,-162.34563)">

                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="3-11" class="level-3_path-11" d="M559.679,99.006L686.2,166.53,566.84,232.316,454.427,172.392l35.59-19.975-0.434-14.764Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/logo-mcdonalds.png"></path>
                    <g class="sale-ballun" transform="translate(123.35424,-113.29327)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="3-12" class="level-3_path-12 disable" d="M545.139,91.19l13.672,7.382L523.654,117.9l4.992-17.8Z"></path>
                    <g class="outer-ballun" transform="translate(100.42698,-185.565)">

                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="3-13" class="level-3_path-13 disable" d="M534.505,96.183l70.1-38.43L559.028,33.219l-70.1,38.647Z"></path>
                    <g class="outer-ballun toilet-ballun" transform="translate(114.24277,-219.77553)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#toilet"></use>
                    </g>
                    <path data-area-target="3-14" class="level-3_path-14" d="M503.906,96.183l-2.821,14.33-96.354,52.76,1.953,8.033-32.769-2.171-97.222-51.891,104.6-57.319,42.752,23.015,27.127-14.764Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/niama.png"></path>
                    <g class="sale-ballun" transform="translate(-45.071574,-157.74037)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <g class="outer-ballun escalator-ballun" transform="translate(-192.11111,24.545667)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#escalator"></use>
                    </g>
                    <g class="outer-ballun escalator-ballun" transform="translate(18.16012,137.12451)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#escalator"></use>
                    </g>
                    <g class="outer-ballun bank-ballun" transform="translate(-247.55923,0.0174)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#bank"></use>
                    </g>
                    <g class="outer-ballun exit-ballun" transform="translate(40.489808,168.75823)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#exit"></use>
                    </g>
                    <g class="outer-ballun exit-ballun" transform="translate(-113.9572,89.673918)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#exit"></use>
                    </g>
                    <g class="outer-ballun exit-ballun" transform="translate(-281.42986,-2.4360448)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#exit"></use>
                    </g>
                    <g class="outer-ballun exit-ballun" transform="translate(47.933033,-207.12485)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#exit"></use>
                    </g>
                    <g class="outer-ballun exit-ballun" transform="translate(254.27053,41.601737)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#exit"></use>
                    </g>
                </svg>
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/lvl-3.png" alt="">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:news",
                    "rent",
                    Array(
                        "ADD_ELEMENT_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "AJAX_MODE" => "Y",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "BROWSER_TITLE" => "-",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "COMPONENT_TEMPLATE" => "rent",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                        "DETAIL_DISPLAY_TOP_PAGER" => "N",
                        "DETAIL_FIELD_CODE" => array(0=>"",1=>"",),
                        "DETAIL_PAGER_SHOW_ALL" => "Y",
                        "DETAIL_PAGER_TEMPLATE" => "",
                        "DETAIL_PAGER_TITLE" => "Страница",
                        "DETAIL_PROPERTY_CODE" => array(0=>"CATEGORY",1=>"MARKET",2=>"LOGO",3=>"",),
                        "DETAIL_SET_CANONICAL_URL" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FILTER_FIELD_CODE" => array(0=>"",1=>"",),
                        "FILTER_NAME" => "",
                        "FILTER_PROPERTY_CODE" => array(0=>"CATEGORY",1=>"",),
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => "8",
                        "IBLOCK_TYPE" => "rent",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "LIST_FIELD_CODE" => array(0=>"",1=>"",),
                        "LIST_PROPERTY_CODE" => array(0=>"CATEGORY",1=>"MARKET",2=>"LOGO",3=>"",),
                        "MESSAGE_404" => "",
                        "META_DESCRIPTION" => "-",
                        "META_KEYWORDS" => "-",
                        "NEWS_COUNT" => "100",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "SEF_MODE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "Y",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N",
                        "USE_CATEGORIES" => "N",
                        "USE_FILTER" => "N",
                        "USE_PERMISSIONS" => "N",
                        "USE_RATING" => "N",
                        "USE_RSS" => "N",
                        "USE_SEARCH" => "N",
                        "USE_SHARE" => "N",
                        "VARIABLE_ALIASES" => array("SECTION_ID"=>"/sheme/","ELEMENT_ID"=>"ELEMENT_ID",)
                        )
                );?>
                <?$APPLICATION->IncludeComponent("bitrix:news", "filter-3", Array(
                    "ADD_ELEMENT_CHAIN" => "N", // Включать название элемента в цепочку навигации
                        "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                        "AJAX_MODE" => "Y", // Включить режим AJAX
                        "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
                        "AJAX_OPTION_HISTORY" => "N",   // Включить эмуляцию навигации браузера
                        "AJAX_OPTION_JUMP" => "N",  // Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
                        "BROWSER_TITLE" => "-", // Установить заголовок окна браузера из свойства
                        "CACHE_FILTER" => "N",  // Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",  // Учитывать права доступа
                        "CACHE_TIME" => "36000000", // Время кеширования (сек.)
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CHECK_DATES" => "Y",   // Показывать только активные на данный момент элементы
                        "COMPOSITE_FRAME_MODE" => "A",  // Голосование шаблона компонента по умолчанию
                        "COMPOSITE_FRAME_TYPE" => "AUTO",   // Содержимое компонента
                        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
                        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",   // Выводить под списком
                        "DETAIL_DISPLAY_TOP_PAGER" => "N",  // Выводить над списком
                        "DETAIL_FIELD_CODE" => array(   // Поля
                            0 => "",
                            1 => "",
                        ),
                        "DETAIL_PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
                        "DETAIL_PAGER_TEMPLATE" => "",  // Название шаблона
                        "DETAIL_PAGER_TITLE" => "Страница", // Название категорий
                        "DETAIL_PROPERTY_CODE" => array(    // Свойства
                            0 => "CATEGORY",
                            1 => "MARKET",
                            2 => "LOGO",
                            3 => "",
                        ),
                        "DETAIL_SET_CANONICAL_URL" => "N",  // Устанавливать канонический URL
                        "DISPLAY_BOTTOM_PAGER" => "Y",  // Выводить под списком
                        "DISPLAY_DATE" => "Y",  // Выводить дату элемента
                        "DISPLAY_NAME" => "Y",  // Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",   // Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",  // Выводить текст анонса
                        "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
                        "FILTER_FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "FILTER_NAME" => "",
                        "FILTER_PROPERTY_CODE" => array(
                            0 => "CATEGORY",
                            1 => "",
                        ),
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",  // Скрывать ссылку, если нет детального описания
                        "IBLOCK_ID" => "8", // Инфоблок
                        "IBLOCK_TYPE" => "rent",   // Тип инфоблока
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
                        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",   // Формат показа даты
                        "LIST_FIELD_CODE" => array( // Поля
                            0 => "",
                            1 => "",
                        ),
                        "LIST_PROPERTY_CODE" => array(  // Свойства
                            0 => "CATEGORY",
                            1 => "MARKET",
                            2 => "LOGO",
                            3 => "",
                        ),
                        "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                        "META_DESCRIPTION" => "-",  // Установить описание страницы из свойства
                        "META_KEYWORDS" => "-", // Установить ключевые слова страницы из свойства
                        "NEWS_COUNT" => "100",  // Количество новостей на странице
                        "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                        "PAGER_DESC_NUMBERING" => "N",  // Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",   // Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                        "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
                        "PAGER_TEMPLATE" => ".default", // Шаблон постраничной навигации
                        "PAGER_TITLE" => "Новости", // Название категорий
                        "PREVIEW_TRUNCATE_LEN" => "",   // Максимальная длина анонса для вывода (только для типа текст)
                        "SEF_MODE" => "N",  // Включить поддержку ЧПУ
                        "SET_LAST_MODIFIED" => "N", // Устанавливать в заголовках ответа время модификации страницы
                        "SET_STATUS_404" => "N",    // Устанавливать статус 404
                        "SET_TITLE" => "Y", // Устанавливать заголовок страницы
                        "SHOW_404" => "N",  // Показ специальной страницы
                        "SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
                        "SORT_BY2" => "SORT",   // Поле для второй сортировки новостей
                        "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                        "SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
                        "STRICT_SECTION_CHECK" => "N",  // Строгая проверка раздела
                        "USE_CATEGORIES" => "N",    // Выводить материалы по теме
                        "USE_FILTER" => "N",    // Показывать фильтр
                        "USE_PERMISSIONS" => "N",   // Использовать дополнительное ограничение доступа
                        "USE_RATING" => "N",    // Разрешить голосование
                        "USE_RSS" => "N",   // Разрешить RSS
                        "USE_SEARCH" => "N",    // Разрешить поиск
                        "USE_SHARE" => "N", // Отображать панель соц. закладок
                        "COMPONENT_TEMPLATE" => ".default",
                        "VARIABLE_ALIASES" => array(
                            "SECTION_ID" => "SECTION_ID",
                            "ELEMENT_ID" => "ELEMENT_ID",
                        )
                    ),
                    false
                );?>  
            </div>
            <div class="main-level main-level-2">
                <svg viewBox="0 0 1000 553">
                    <path data-area-target="2-1" class="level-2_path-1" d="M890.128,292.294l25.258-13.9,82.72,44.223L964.428,341.15,897.5,305.35Z"></path>
                    <g class="sale-ballun" transform="translate(502.65776,19.657809)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-2" class="level-2_path-2" d="M897.074,305.982l-36.2,20.006,66.723,35.589,35.993-20.006Z" data-area-target="BERSHKA2" data-fancybox data-src="#clothing-area"></path>
                    <g class="sale-ballun" transform="translate(480.32807,49.430725)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-3" class="level-2_path-3" d="M859.819,326.2L809.3,354.206,875.816,389.8,926.542,362Z"></path>
                    <g class="sale-ballun" transform="translate(437.5295,69.899606)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-4" class="level-2_path-4" d="M808.461,354.628l-38.307,21.058,66.512,35.8,38.308-21.269Z"></path>
                    <g class="sale-ballun" transform="translate(390.07891,97.811716)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-5" class="level-2_path-5" d="M769.312,376.318l-18.1,9.687,43.57,23.375,17.891-10.108Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/online-treid.png"></path>
                    <g class="sale-ballun" transform="translate(345.28055,103.32867)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-6" class="level-2_path-6" d="M750.368,386.426l-44.832,24.849,66.933,35.589,44.412-24.638Z"></path>
                    <g class="sale-ballun" transform="translate(321.78786,125.65836)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-7" class="level-2_path-7" d="M704.9,411.486l-20.417,11.372,66.723,35.589,20.206-11.372Z"></path>
                    <g class="sale-ballun" transform="translate(288.75853,143.56863)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-8" class="level-2_path-8" d="M683.856,423.068l-29.047,16,66.513,35.8,29.046-16.215Z"></path>
                    <g class="sale-ballun" transform="translate(264.59977,158.82147)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-9" class="level-2_path-9" d="M653.757,439.7l-39.15,21.48,66.513,35.589,39.36-21.479Z"></path>
                    <g class="sale-ballun" transform="translate(233.66385,179.29035)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-10" class="level-2_path-10 disable" d="M679.857,496.984L638.6,499.3l-96.4-51.383,22.521-12.424Z"></path>
                    <g class="outer-ballun" transform="translate(183.18945,188.36179)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="2-11" class="level-2_path-11" d="M637.55,499.932l-71.143,38.959L426.226,463.922l51.779-28.429,13.47,7.16,26.942,1.474,7.367-4Z"></path>
                    <g class="sale-ballun" transform="translate(99.35977,201.23547)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-12" class="level-2_path-12" d="M476.952,435.072l-51.568,28.639-70.932-37.905,51.778-28.64Z"></path>
                    <g class="sale-ballun" transform="translate(-22.02181,139.72231)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-13" class="level-2_path-13" d="M405.388,396.745L353.4,425.385l-68.827-36.853,51.778-28.64Z"></path>
                    <g class="sale-ballun" transform="translate(-88.798126,104.52494)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-14" class="level-2_path-14" d="M335.508,359.471l-51.778,28.64-69.038-36.853,51.778-28.64Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/Reiker.png"></path>
                    <g class="sale-ballun" transform="translate(-161.16655,67.353887)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-15" class="level-2_path-15" d="M265.418,322.408L231.741,304.3l-51.568,28.43,33.677,18.11Z"></path>
                    <g class="sale-ballun" transform="translate(-216.42971,38.735466)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-16" class="level-2_path-16" d="M230.9,303.876l-51.568,28.429-34.519-18.321,51.778-28.429Z"></path>
                    <g class="sale-ballun" transform="translate(-248.3376,20.314413)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-17" class="level-2_path-17" d="M195.538,284.923l-51.568,28.43-34.1-17.9,52.2-28.64Z"></path>
                    <g class="sale-ballun" transform="translate(-286.82444,1.8933603)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-18" class="level-2_path-18" d="M180.173,255.863L109.24,295.032,1.894,237.752l70.932-39.169Z"></path>
                    <g class="sale-ballun" transform="translate(-345.21602,-35.097214)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-19" class="level-2_path-19 disable" d="M185.645,257.758l22.311-12.425L77.457,175.84l-3.789,22.322Z"></path>
                    <g class="outer-ballun" transform="translate(-296.95747,-69.924979)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="2-20" class="level-2_path-20" d="M207.746,244.28l6.735-3.79-23.363-12.425-6.946,3.791Z"></path>
                    <g class="sale-ballun" transform="translate(-240.77181,-55.014535)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-21" class="level-2_path-21" d="M265.418,271.025l0.421-.211,12.418,6.528-3.157,2.106Z"></path>
                    <g class="sale-ballun" transform="translate(-170.04813,-13.238219)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-22" class="level-2_path-22" d="M280.993,284.5l5.262-2.948,12.419,6.739-7.788,4.422Z"></path>
                    <g class="sale-ballun" transform="translate(-152.92823,-0.88353)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-23" class="level-2_path-23" d="M328.773,334.411l16.838-9.476,8.42,4.633-16.839,9.476Z"></path>
                    <g class="sale-ballun" transform="translate(-96.020335,40.892786)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-24" class="level-2_path-24" d="M355.715,329.568l10.1-5.475L407.7,346.415,397.6,352.1Z"></path>
                    <g class="sale-ballun" transform="translate(-58.191388,47.471733)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-25" class="level-2_path-25" d="M456.325,372.106L489.16,389.8l-27.152-5.685-3.368-1.053-3.578-1.264-3.157-1.263-5.683-2.738Z"></path>
                    <g class="sale-ballun" transform="translate(34.242823,95.826996)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-26" class="level-2_path-26" d="M506.841,410.012l7.366-4.212,0.842,0.632h0.632l0.631-.421,0.632-.422,0.21-.842,1.263,0.632L508.1,411.065Z"></path>
                    <g class="sale-ballun" transform="">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-27" class="level-2_path-27" d="M582.4,443.916l6.946-3.79,14.1,7.791-6.946,3.58Z"></path>
                    <g class="sale-ballun" transform="translate(154.29398,155.84073)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-28" class="level-2_path-28" d="M582.614,436.756l-6.946,3.791-13.892-7.581,6.736-4Z"></path>
                    <g class="sale-ballun" transform="translate(133.24135,143.99862)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-29" class="level-2_path-29" d="M78.089,175.208L113.871,155.2l66.723,35.589L144.812,210.8Z"></path>
                    <g class="sale-ballun" transform="translate(-304.45645,-99.390258)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-30" class="level-2_path-30" d="M115.134,154.781l66.512,35.379,68.2-37.274-66.723-35.8Z"></path>
                    <g class="sale-ballun" transform="translate(-257.17723,-135.57853)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-31" class="level-2_path-31" d="M213.85,100.45l-30.1,16.215,66.933,35.8,29.678-16.426Z"></path>
                    <g class="sale-ballun" transform="translate(-210.27623,-163.06453)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-32" class="level-2_path-32" d="M237.634,112.243l18.312-9.687,43.359,22.954-18.1,10.108Z"></path>
                    <g class="sale-ballun" transform="translate(-169.37623,-177.82353)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-33" class="level-2_path-33" d="M300.147,124.878L233.424,89.5l38.1-21.059,66.723,35.589Z"></path>
                    <g class="sale-ballun" transform="translate(-147.95723,-190.24853)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-34" class="level-2_path-34" d="M339.3,103.609l21.469-12L294.043,56.016l-21.258,12Z"></path>
                    <g class="sale-ballun" transform="translate(-118.70023,-206.04253)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-35" class="level-2_path-35" d="M361.819,91.184l28.2-15.583L323.3,40.222,295.1,55.805Z"></path>
                    <g class="sale-ballun" transform="translate(-92.648928,-219.99858)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-36" class="level-2_path-36" d="M390.865,75.39l-66.723-35.8,35.993-19.8,67.143,35.8Z"></path>
                    <g class="sale-ballun" transform="translate(-58.223993,-234.88504)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-37" class="level-2_path-37" d="M361.187,19.374L394.864,0.842l82.72,44.223-25.048,13.9-24.416-4Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/scandimoda.png"></path>
                    <g class="sale-ballun" transform="translate(2.5767537,-237.09)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-38" class="level-2_path-38 disable" d="M453.589,59.385L515.681,25.06,554.62,45.908,492.738,80.023Z"></path>
                    <g class="outer-ballun" transform="translate(71.98077,-231.70453)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="2-39" class="level-2_path-39" d="M482.425,75.6l20.416,10.74-17.259,9.687-20.417-10.74Z"></path>
                    <g class="sale-ballun" transform="translate(44.78377,-203.89953)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-40" class="level-2_path-40 disable" d="M493.58,80.444l39.15,21.269,62.3-34.115L555.672,46.329Z"></path>
                    <g class="outer-ballun" transform="translate(97.71977,-205.85653)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="2-41" class="level-2_path-41" d="M530.625,103.819L558.83,88.236,628.078,125.3l-27.994,15.583h-30.52l-38.728-20.637Z"></path>
                    <g class="sale-ballun" transform="translate(134.93592,-171.43159)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-42" class="level-2_path-42" d="M600.926,141.3l28.2-15.583,66.933,35.8-43.149,23.8-51.778-27.587Z"></path>
                    <g class="sale-ballun" transform="translate(205.6466,-134.21544)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-43" class="level-2_path-43" d="M653.757,185.948l43.149-24.007,66.933,35.8-28.2,15.372-30.31.211Z"></path>
                    <g class="sale-ballun" transform="translate(272.63566,-97.929697)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-44" class="level-2_path-44" d="M736.9,213.535l28-15.373,69.248,37.063L806.357,250.6h-30.52l-38.729-20.427Z"></path>
                    <g class="sale-ballun" transform="translate(340.55513,-62.574358)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-45" class="level-2_path-45 disable" d="M810.145,249.545l62.092-34.326,38.94,20.848L849.3,270.393Z"></path>
                    <g class="outer-ballun" transform="translate(428.53677,-41.54553)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="2-46" class="level-2_path-46 disable" d="M850.137,270.6l39.57,21.058L951.8,257.547l-39.571-21.058Z"></path>
                    <g class="outer-ballun" transform="translate(454.27677,-15.69753)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="2-47" class="level-2_path-47" d="M760.682,266.6l81.035,43.381-45.885,25.06-80.825-43.17Z"></path>
                    <g class="sale-ballun" transform="translate(339.16271,12.277987)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-48" class="level-2_path-48" d="M714.165,292.294l80.825,43.17-45.464,25.481L668.28,317.354Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/pegas.png"></path>
                    <g class="sale-ballun" transform="translate(293.10773,37.864088)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-49" class="level-2_path-49 disable" d="M653.757,339.255l26.31-14.531,68.407,36.221-26.521,14.741Z"></path>
                    <g class="outer-ballun" transform="translate(261.47401,61.589381)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="2-50" class="level-2_path-50" d="M720.69,376.107l-58.3,32.641-24.416-13.267-42.307,23.165-56.409-30.325,100.821-55.384Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/Domani.png"></path>
                    <g class="sale-ballun" transform="translate(189.36773,89.501491)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-51" class="level-2_path-51" d="M479.478,356.733l48.832-27.376,33.466,17.9-48.831,27.376Z"></path>
                    <g class="sale-ballun" transform="translate(82.70977,60.89247)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-52" class="level-2_path-52" d="M478.215,355.891l49.042-26.955L494,311.247l-49.042,26.744Z"></path>
                    <g class="sale-ballun" transform="translate(49.45377,42.15047)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-53" class="level-2_path-53" d="M443.7,337.57l49.042-26.955-33.677-18.321L410.019,319.46Z"></path>
                    <g class="sale-ballun" transform="translate(12.61677,23.61847)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-54" class="level-2_path-54" d="M340.56,282.4L389.6,255.441l68.617,36.642-49.253,26.955Z"></path>
                    <g class="sale-ballun" transform="translate(-38.09023,-1.9676307)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-55" class="level-2_path-55" d="M305.83,264.075l49.043-27.165,33.887,18.11-49.253,26.955Z"></path>
                    <g class="sale-ballun" transform="translate(-93.29323,-31.22453)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-56" class="level-2_path-56" d="M355.925,236.7l50.726-28.008,67.354,36.01L423.49,272.709Z"></path>
                    <g class="sale-ballun" transform="translate(-22.744248,-49.165934)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-57" class="level-2_path-57" d="M424.542,273.131l50.305-28.219,34.94,18.953-50.726,27.8Z"></path>
                    <g class="sale-ballun" transform="translate(28.40477,-23.76253)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-58" class="level-2_path-58" d="M460.324,292.083l50.516-27.8,33.466,17.9L494,309.983Z"></path>
                    <g class="sale-ballun" transform="translate(67.016522,2.2887725)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-59" class="level-2_path-59" d="M495.054,310.615l50.515-27.8,68.2,36.22L563.25,347.046Z"></path>
                    <g class="sale-ballun" transform="translate(116.32792,27.409671)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-60" class="level-2_path-60" d="M199.747,206.8l61.882-34.115,81.036,43.381-62.093,34.115Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/optic-city.jpg"></path>
                    <g class="sale-ballun" transform="translate(-163.1412,-73.448117)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-61" class="level-2_path-61" d="M262.681,172.26l37.256-20.638L380.972,195,343.507,215.64Z"></path>
                    <g class="sale-ballun" transform="translate(-117.08622,-102.75583)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-62" class="level-2_path-62 disable" d="M300.779,151.2l26.52-14.53,68.407,36.22-26.521,14.741Z"></path>
                    <g class="outer-ballun" transform="translate(-90.569716,-127.41153)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="2-64" class="level-2_path-63" d="M328.141,136.039l46.1-25.06,80.825,43.17L409.387,179.42Z"></path>
                    <g class="sale-ballun" transform="translate(-49.166753,-141.83279)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-64" class="level-2_path-64" d="M374.868,110.558l45.675-25.271,39.15,20.849-45.675,25.27Z"></path>
                    <g class="sale-ballun" transform="translate(-17.99823,-180.90974)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-65" class="level-2_path-65" d="M414.86,131.827l45.675-25.27,41.254,21.9L455.9,153.728Z"></path>
                    <g class="sale-ballun" transform="translate(16.330375,-155.3947)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-66" class="level-2_path-66" d="M478.215,169.522l33.467,18.11-47.148,26.113-33.888-18.11Z"></path>
                    <g class="sale-ballun" transform="translate(32.61477,-98.94253)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-67" class="level-2_path-67" d="M465.376,214.166l47.569-26.112,33.466,18.11-47.148,26.113Z"></path>
                    <g class="sale-ballun" transform="translate(67.34477,-80.41053)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-68" class="level-2_path-68" d="M499.9,232.909l47.569-26.324,33.887,17.9-47.779,26.323Z"></path>
                    <g class="sale-ballun" transform="translate(100.81077,-62.30053)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-69" class="level-2_path-69" d="M534.624,251.23l47.359-26.113,33.887,17.9-47.358,26.112Z"></path>
                    <g class="sale-ballun" transform="translate(136.38277,-43.34753)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-70" class="level-2_path-70" d="M569.354,269.551l47.358-26.113,33.467,17.9L602.61,287.661Z"></path>
                    <g class="sale-ballun" transform="translate(171.11177,-25.02653)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-71" class="level-2_path-71" d="M611.45,283.66l39.571-21.9,34.308,18.321-39.781,21.9Z"></path>
                    <g class="sale-ballun" transform="translate(212.0852,-10.146237)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-72" class="level-2_path-72" d="M652.073,261.338l48.411-26.745L734.793,252.7l-48.832,26.744Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/TezTour.png"></path>
                    <g class="sale-ballun" transform="translate(254.88377,-33.87153)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-73" class="level-2_path-73" d="M651.021,260.917l48.411-26.745-67.776-36.431-48.621,26.744Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/ozon.svg"></path>
                    <g class="sale-ballun" transform="translate(205.23894,-58.313728)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-74" class="level-2_path-74" d="M630.815,197.319l-48.622,26.956L479.057,169.1l48.621-26.745Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/ItaIta.png"></path>
                    <g class="sale-ballun" transform="translate(117.31579,-107.15992)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="2-75" class="level-2_path-75" d="M549.148,427.28l14.523,7.792L541.36,447.5l-14.734-8Z"></path>
                    <g class="outer-ballun elevator-ballun" transform="translate(105.6314,147.65172)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#elevator"></use>
                    </g>
                    <g class="outer-ballun escalator-ballun" transform="translate(-171.0074,0.41515241)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#escalator"></use>
                    </g>
                    <g class="outer-ballun escalator-ballun" transform="translate(41.57477,106.2684)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#escalator"></use>
                    </g>
                    <g class="outer-ballun bank-ballun" transform="translate(-126.81323,12.5104)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#bank"></use>
                    </g>
                    <g class="outer-ballun terminal-ballun" transform="translate(101.35477,137.8564)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#terminal"></use>
                    </g>
                    <g class="outer-ballun exit-ballun" transform="translate(-272.68781,-68.212372)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#exit"></use>
                    </g>
                    <g class="outer-ballun exit-ballun" transform="translate(77.09077,-187.74558)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#exit"></use>
                    </g>
                    <g class="outer-ballun exit-ballun" transform="translate(392.14477,-21.9496)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#exit"></use>
                    </g>
                    <g class="outer-ballun exit-ballun" transform="translate(169.55915,166.52646)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#exit"></use>
                    </g>
                </svg>
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/lvl-2-1.png" alt="">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:news",
                    "rent",
                    Array(
                        "ADD_ELEMENT_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "AJAX_MODE" => "Y",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "BROWSER_TITLE" => "-",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "COMPONENT_TEMPLATE" => "rent",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                        "DETAIL_DISPLAY_TOP_PAGER" => "N",
                        "DETAIL_FIELD_CODE" => array(0=>"",1=>"",),
                        "DETAIL_PAGER_SHOW_ALL" => "Y",
                        "DETAIL_PAGER_TEMPLATE" => "",
                        "DETAIL_PAGER_TITLE" => "Страница",
                        "DETAIL_PROPERTY_CODE" => array(0=>"CATEGORY",1=>"MARKET",2=>"LOGO",3=>"",),
                        "DETAIL_SET_CANONICAL_URL" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FILTER_FIELD_CODE" => array(0=>"",1=>"",),
                        "FILTER_NAME" => "",
                        "FILTER_PROPERTY_CODE" => array(0=>"CATEGORY",1=>"",),
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => "10",
                        "IBLOCK_TYPE" => "rent",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "LIST_FIELD_CODE" => array(0=>"",1=>"",),
                        "LIST_PROPERTY_CODE" => array(0=>"CATEGORY",1=>"MARKET",2=>"LOGO",3=>"",),
                        "MESSAGE_404" => "",
                        "META_DESCRIPTION" => "-",
                        "META_KEYWORDS" => "-",
                        "NEWS_COUNT" => "100",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "SEF_MODE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "Y",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N",
                        "USE_CATEGORIES" => "N",
                        "USE_FILTER" => "N",
                        "USE_PERMISSIONS" => "N",
                        "USE_RATING" => "N",
                        "USE_RSS" => "N",
                        "USE_SEARCH" => "N",
                        "USE_SHARE" => "N",
                        "VARIABLE_ALIASES" => array("SECTION_ID"=>"/sheme/","ELEMENT_ID"=>"ELEMENT_ID",)
                        )
                );?>
                <?$APPLICATION->IncludeComponent("bitrix:news", "filter-2", Array(
                    "ADD_ELEMENT_CHAIN" => "N", // Включать название элемента в цепочку навигации
                        "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                        "AJAX_MODE" => "Y", // Включить режим AJAX
                        "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
                        "AJAX_OPTION_HISTORY" => "N",   // Включить эмуляцию навигации браузера
                        "AJAX_OPTION_JUMP" => "N",  // Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
                        "BROWSER_TITLE" => "-", // Установить заголовок окна браузера из свойства
                        "CACHE_FILTER" => "N",  // Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",  // Учитывать права доступа
                        "CACHE_TIME" => "36000000", // Время кеширования (сек.)
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CHECK_DATES" => "Y",   // Показывать только активные на данный момент элементы
                        "COMPOSITE_FRAME_MODE" => "A",  // Голосование шаблона компонента по умолчанию
                        "COMPOSITE_FRAME_TYPE" => "AUTO",   // Содержимое компонента
                        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
                        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",   // Выводить под списком
                        "DETAIL_DISPLAY_TOP_PAGER" => "N",  // Выводить над списком
                        "DETAIL_FIELD_CODE" => array(   // Поля
                            0 => "",
                            1 => "",
                        ),
                        "DETAIL_PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
                        "DETAIL_PAGER_TEMPLATE" => "",  // Название шаблона
                        "DETAIL_PAGER_TITLE" => "Страница", // Название категорий
                        "DETAIL_PROPERTY_CODE" => array(    // Свойства
                            0 => "CATEGORY",
                            1 => "MARKET",
                            2 => "LOGO",
                            3 => "",
                        ),
                        "DETAIL_SET_CANONICAL_URL" => "N",  // Устанавливать канонический URL
                        "DISPLAY_BOTTOM_PAGER" => "Y",  // Выводить под списком
                        "DISPLAY_DATE" => "Y",  // Выводить дату элемента
                        "DISPLAY_NAME" => "Y",  // Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",   // Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",  // Выводить текст анонса
                        "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
                        "FILTER_FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "FILTER_NAME" => "",
                        "FILTER_PROPERTY_CODE" => array(
                            0 => "CATEGORY",
                            1 => "",
                        ),
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",  // Скрывать ссылку, если нет детального описания
                        "IBLOCK_ID" => "10", // Инфоблок
                        "IBLOCK_TYPE" => "rent",   // Тип инфоблока
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
                        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",   // Формат показа даты
                        "LIST_FIELD_CODE" => array( // Поля
                            0 => "",
                            1 => "",
                        ),
                        "LIST_PROPERTY_CODE" => array(  // Свойства
                            0 => "CATEGORY",
                            1 => "MARKET",
                            2 => "LOGO",
                            3 => "",
                        ),
                        "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                        "META_DESCRIPTION" => "-",  // Установить описание страницы из свойства
                        "META_KEYWORDS" => "-", // Установить ключевые слова страницы из свойства
                        "NEWS_COUNT" => "100",  // Количество новостей на странице
                        "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                        "PAGER_DESC_NUMBERING" => "N",  // Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",   // Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                        "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
                        "PAGER_TEMPLATE" => ".default", // Шаблон постраничной навигации
                        "PAGER_TITLE" => "Новости", // Название категорий
                        "PREVIEW_TRUNCATE_LEN" => "",   // Максимальная длина анонса для вывода (только для типа текст)
                        "SEF_MODE" => "N",  // Включить поддержку ЧПУ
                        "SET_LAST_MODIFIED" => "N", // Устанавливать в заголовках ответа время модификации страницы
                        "SET_STATUS_404" => "N",    // Устанавливать статус 404
                        "SET_TITLE" => "Y", // Устанавливать заголовок страницы
                        "SHOW_404" => "N",  // Показ специальной страницы
                        "SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
                        "SORT_BY2" => "SORT",   // Поле для второй сортировки новостей
                        "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                        "SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
                        "STRICT_SECTION_CHECK" => "N",  // Строгая проверка раздела
                        "USE_CATEGORIES" => "N",    // Выводить материалы по теме
                        "USE_FILTER" => "N",    // Показывать фильтр
                        "USE_PERMISSIONS" => "N",   // Использовать дополнительное ограничение доступа
                        "USE_RATING" => "N",    // Разрешить голосование
                        "USE_RSS" => "N",   // Разрешить RSS
                        "USE_SEARCH" => "N",    // Разрешить поиск
                        "USE_SHARE" => "N", // Отображать панель соц. закладок
                        "COMPONENT_TEMPLATE" => ".default",
                        "VARIABLE_ALIASES" => array(
                            "SECTION_ID" => "SECTION_ID",
                            "ELEMENT_ID" => "ELEMENT_ID",
                        )
                    ),
                    false
                );?>
            </div>
            <div class="main-level main-level-1 active">
                <svg viewBox="0 0 1000 553">
                    <path class="level-1_path-1" d="M998.272,313.871l-150.789,82.95-52.495-28.082-22.467,12.529-23.979-12.961,22.251-12.313L642.687,287.733l102.182-56.38,27,14.473,20.955-11.665,29.6,15.769,27.652-15.121Z"
                    data-src="#clothing-area" data-area-target="1-1" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/fancy.png"></path>
                    <g class="sale-ballun" transform="translate(387.04792,34.424938)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path class="level-1_path-2" d="M588.032,282.98L761.5,187.718,411.1,0.864,260.315,83.814,311.3,111.248l-22.251,12.313,24.844,13.393,22.683-12.529L446.965,183.4l-22.251,12.529Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/logo-perekrestok.png" data-area-target="1-2"></path>
                    <g class="sale-ballun" transform="translate(66.05866,-147.00378)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path class="level-1_path-3" data-area-target="1-3" d="M243.465,119.025L270.9,133.714l-23.979,13.393L219.7,132.418Z"></path>
                    <g class="sale-ballun" transform="translate(-193.01259,-155.98351)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                </svg>
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/lvl-1.png" alt="">
                <?$APPLICATION->IncludeComponent("bitrix:news", "rent", Array(
                    "ADD_ELEMENT_CHAIN" => "N", // Включать название элемента в цепочку навигации
                        "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                        "AJAX_MODE" => "Y", // Включить режим AJAX
                        "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
                        "AJAX_OPTION_HISTORY" => "N",   // Включить эмуляцию навигации браузера
                        "AJAX_OPTION_JUMP" => "N",  // Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
                        "BROWSER_TITLE" => "-", // Установить заголовок окна браузера из свойства
                        "CACHE_FILTER" => "N",  // Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",  // Учитывать права доступа
                        "CACHE_TIME" => "36000000", // Время кеширования (сек.)
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CHECK_DATES" => "Y",   // Показывать только активные на данный момент элементы
                        "COMPOSITE_FRAME_MODE" => "A",  // Голосование шаблона компонента по умолчанию
                        "COMPOSITE_FRAME_TYPE" => "AUTO",   // Содержимое компонента
                        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
                        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",   // Выводить под списком
                        "DETAIL_DISPLAY_TOP_PAGER" => "N",  // Выводить над списком
                        "DETAIL_FIELD_CODE" => array(   // Поля
                            0 => "",
                            1 => "",
                        ),
                        "DETAIL_PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
                        "DETAIL_PAGER_TEMPLATE" => "",  // Название шаблона
                        "DETAIL_PAGER_TITLE" => "Страница", // Название категорий
                        "DETAIL_PROPERTY_CODE" => array(    // Свойства
                            0 => "CATEGORY",
                            1 => "MARKET",
                            2 => "LOGO",
                            3 => "",
                        ),
                        "DETAIL_SET_CANONICAL_URL" => "N",  // Устанавливать канонический URL
                        "DISPLAY_BOTTOM_PAGER" => "Y",  // Выводить под списком
                        "DISPLAY_DATE" => "Y",  // Выводить дату элемента
                        "DISPLAY_NAME" => "Y",  // Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",   // Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",  // Выводить текст анонса
                        "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
                        "FILTER_FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "FILTER_NAME" => "",
                        "FILTER_PROPERTY_CODE" => array(
                            0 => "CATEGORY",
                            1 => "",
                        ),
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",  // Скрывать ссылку, если нет детального описания
                        "IBLOCK_ID" => "7", // Инфоблок
                        "IBLOCK_TYPE" => "rent",   // Тип инфоблока
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
                        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",   // Формат показа даты
                        "LIST_FIELD_CODE" => array( // Поля
                            0 => "",
                            1 => "",
                        ),
                        "LIST_PROPERTY_CODE" => array(  // Свойства
                            0 => "CATEGORY",
                            1 => "MARKET",
                            2 => "LOGO",
                            3 => "",
                        ),
                        "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                        "META_DESCRIPTION" => "-",  // Установить описание страницы из свойства
                        "META_KEYWORDS" => "-", // Установить ключевые слова страницы из свойства
                        "NEWS_COUNT" => "100",  // Количество новостей на странице
                        "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                        "PAGER_DESC_NUMBERING" => "N",  // Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",   // Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                        "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
                        "PAGER_TEMPLATE" => ".default", // Шаблон постраничной навигации
                        "PAGER_TITLE" => "Новости", // Название категорий
                        "PREVIEW_TRUNCATE_LEN" => "",   // Максимальная длина анонса для вывода (только для типа текст)
                        "SEF_MODE" => "N",  // Включить поддержку ЧПУ
                        "SET_LAST_MODIFIED" => "N", // Устанавливать в заголовках ответа время модификации страницы
                        "SET_STATUS_404" => "N",    // Устанавливать статус 404
                        "SET_TITLE" => "Y", // Устанавливать заголовок страницы
                        "SHOW_404" => "N",  // Показ специальной страницы
                        "SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
                        "SORT_BY2" => "SORT",   // Поле для второй сортировки новостей
                        "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                        "SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
                        "STRICT_SECTION_CHECK" => "N",  // Строгая проверка раздела
                        "USE_CATEGORIES" => "N",    // Выводить материалы по теме
                        "USE_FILTER" => "N",    // Показывать фильтр
                        "USE_PERMISSIONS" => "N",   // Использовать дополнительное ограничение доступа
                        "USE_RATING" => "N",    // Разрешить голосование
                        "USE_RSS" => "N",   // Разрешить RSS
                        "USE_SEARCH" => "N",    // Разрешить поиск
                        "USE_SHARE" => "N", // Отображать панель соц. закладок
                        "COMPONENT_TEMPLATE" => ".default",
                        "VARIABLE_ALIASES" => array("SECTION_ID"=>"/sheme/","ELEMENT_ID"=>"ELEMENT_ID",)
                        )
                );?>
                <?$APPLICATION->IncludeComponent("bitrix:news", "filter", Array(
                    "ADD_ELEMENT_CHAIN" => "N", // Включать название элемента в цепочку навигации
                        "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                        "AJAX_MODE" => "Y", // Включить режим AJAX
                        "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
                        "AJAX_OPTION_HISTORY" => "N",   // Включить эмуляцию навигации браузера
                        "AJAX_OPTION_JUMP" => "N",  // Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
                        "BROWSER_TITLE" => "-", // Установить заголовок окна браузера из свойства
                        "CACHE_FILTER" => "N",  // Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",  // Учитывать права доступа
                        "CACHE_TIME" => "36000000", // Время кеширования (сек.)
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CHECK_DATES" => "Y",   // Показывать только активные на данный момент элементы
                        "COMPOSITE_FRAME_MODE" => "A",  // Голосование шаблона компонента по умолчанию
                        "COMPOSITE_FRAME_TYPE" => "AUTO",   // Содержимое компонента
                        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
                        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",   // Выводить под списком
                        "DETAIL_DISPLAY_TOP_PAGER" => "N",  // Выводить над списком
                        "DETAIL_FIELD_CODE" => array(   // Поля
                            0 => "",
                            1 => "",
                        ),
                        "DETAIL_PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
                        "DETAIL_PAGER_TEMPLATE" => "",  // Название шаблона
                        "DETAIL_PAGER_TITLE" => "Страница", // Название категорий
                        "DETAIL_PROPERTY_CODE" => array(    // Свойства
                            0 => "CATEGORY",
                            1 => "MARKET",
                            2 => "LOGO",
                            3 => "",
                        ),
                        "DETAIL_SET_CANONICAL_URL" => "N",  // Устанавливать канонический URL
                        "DISPLAY_BOTTOM_PAGER" => "Y",  // Выводить под списком
                        "DISPLAY_DATE" => "Y",  // Выводить дату элемента
                        "DISPLAY_NAME" => "Y",  // Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",   // Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",  // Выводить текст анонса
                        "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
                        "FILTER_FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "FILTER_NAME" => "",
                        "FILTER_PROPERTY_CODE" => array(
                            0 => "CATEGORY",
                            1 => "",
                        ),
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",  // Скрывать ссылку, если нет детального описания
                        "IBLOCK_ID" => "7", // Инфоблок
                        "IBLOCK_TYPE" => "rent",   // Тип инфоблока
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
                        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",   // Формат показа даты
                        "LIST_FIELD_CODE" => array( // Поля
                            0 => "",
                            1 => "",
                        ),
                        "LIST_PROPERTY_CODE" => array(  // Свойства
                            0 => "CATEGORY",
                            1 => "MARKET",
                            2 => "LOGO",
                            3 => "",
                        ),
                        "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                        "META_DESCRIPTION" => "-",  // Установить описание страницы из свойства
                        "META_KEYWORDS" => "-", // Установить ключевые слова страницы из свойства
                        "NEWS_COUNT" => "100",  // Количество новостей на странице
                        "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                        "PAGER_DESC_NUMBERING" => "N",  // Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",   // Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                        "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
                        "PAGER_TEMPLATE" => ".default", // Шаблон постраничной навигации
                        "PAGER_TITLE" => "Новости", // Название категорий
                        "PREVIEW_TRUNCATE_LEN" => "",   // Максимальная длина анонса для вывода (только для типа текст)
                        "SEF_MODE" => "N",  // Включить поддержку ЧПУ
                        "SET_LAST_MODIFIED" => "N", // Устанавливать в заголовках ответа время модификации страницы
                        "SET_STATUS_404" => "N",    // Устанавливать статус 404
                        "SET_TITLE" => "Y", // Устанавливать заголовок страницы
                        "SHOW_404" => "N",  // Показ специальной страницы
                        "SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
                        "SORT_BY2" => "SORT",   // Поле для второй сортировки новостей
                        "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                        "SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
                        "STRICT_SECTION_CHECK" => "N",  // Строгая проверка раздела
                        "USE_CATEGORIES" => "N",    // Выводить материалы по теме
                        "USE_FILTER" => "N",    // Показывать фильтр
                        "USE_PERMISSIONS" => "N",   // Использовать дополнительное ограничение доступа
                        "USE_RATING" => "N",    // Разрешить голосование
                        "USE_RSS" => "N",   // Разрешить RSS
                        "USE_SEARCH" => "N",    // Разрешить поиск
                        "USE_SHARE" => "N", // Отображать панель соц. закладок
                        "COMPONENT_TEMPLATE" => ".default",
                        "VARIABLE_ALIASES" => array(
                            "SECTION_ID" => "SECTION_ID",
                            "ELEMENT_ID" => "ELEMENT_ID",
                        )
                    ),
                    false
                );?>
            </div>
            <div class="main-level main-level-0">
                <svg viewBox="0 0 1000 553">
                    <path data-area-target="0-1" class="level-0_path-1 disable" d="M10.661,354.459l54.83-30.227,45.039,23.921L72.672,369.247l-15.448-8.481-17.189,9.568Zm0,0,54.83-30.227,45.039,23.921L72.672,369.247l-15.448-8.481-17.189,9.568Z"></path>
                    <g class="outer-ballun" transform="translate(-389.27337,62.373835)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="0-2" class="level-0_path-2" d="M73.542,369.464l169.931-93.073-20.018-10.873,9.139-4.784,15.448,12.613,10.226,5.436,18.494-10.22,16.536,8.7,72.455-40.012,224.108,119.6L534.6,387.513l22.846,11.96-41.558,22.834-24.8-13.265-169.06,93.072L74.195,369.9Z" data-logo-src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/korablik.png"></path>
                    <g class="sale-ballun" transform="translate(-108.29146,95.868367)">
                        <use x="0" y="0" xlink:href="#ballun-body"></use>
                        <circle cx="438.90021" cy="251.96454" r="12.5" style="fill:#19a69c;"></circle>
                        <use x="0" y="0" xlink:href="#fill-white"></use>
                    </g>
                    <path data-area-target="0-3" class="level-0_path-3 disable" d="M111.4,347.718l129.9-71.327-19.583-10.655,10.227-5.654-11.1-9.568v-2.827l18.277-10.221,5,2.827v1.957l5.44,3.045v1.3l3.481,1.74,8.268-4.567-45.692-24.355,17.407-9.786-6.963-3.914,59.617-32.837L385.77,226.158l-18.929,10.656L667.755,397.081l-98.782,54.583-77.894-41.535L361.836,481.238l44.6,23.7,58.094-32.184L587.685,538.43,998.259,313.142l-72.019-38.49,59.182-32.4-84.639-45.232-59.4,32.619L568.973,84.374l59.182-32.4L543.516,6.741l-59.4,32.836-71.8-38.49L1.958,226.593l122.716,65.238L66.58,324.015Z"></path>
                    <g class="outer-ballun" transform="translate(126.17026,-58.578641)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="0-4" class="level-0_path-4 disable" d="M323.107,502.332l37.859-20.876,44.386,23.92L350.522,535.6l-29.591-15.874,16.754-9.351Z"></path>
                    <g class="outer-ballun" transform="translate(-78.73423,226.79147)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="0-5" class="level-0_path-5" d="M552.219,441.443l24.8-13.7L594,436.877l-25.022,13.7Z"></path>
                    <g class="outer-ballun elevator-ballun" transform="translate(131.88261,150.50847)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#elevator"></use>
                    </g>
                    <path data-area-target="0-6" class="level-0_path-6 disable" d="M594.647,436.442l15.884-8.7L593.56,418.61l-15.449,8.7Z"></path>
                    <g class="outer-ballun" transform="translate(156.88261,137.35058)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="0-7" class="level-0_path-7 disable" d="M611.4,427.308L665.8,397.3l-75.065-40.013L536.336,387.3Z"></path>
                    <g class="outer-ballun" transform="translate(157.93477,108.32247)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="0-8" class="level-0_path-8 disable" d="M526.98,428.178L558.312,411l24.151,12.83-31.114,17.4Z"></path>
                    <g class="outer-ballun" transform="translate(109.49977,137.93547)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <path data-area-target="0-9" class="level-0_path-9 disable" d="M227.807,205.717l57.659-31.749,98.346,52.408L293.3,276.391l-16.536-8.916-0.217-.435-5.44-2.826v-1.305l-5-2.827-0.218-.218v-1.3l-5.439-2.827v-1.74L255,251.166v-1.74l-1.088-.435,9.356-5.219L217.58,219.2l17.407-9.785Z"></path>
                    <g class="outer-ballun" transform="translate(-144.44759,-54.827688)">
                        <use xlink:href="#ballun"></use>
                    </g>
                    <g class="outer-ballun exit-ballun" transform="translate(-371.70323,68.8604)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#exit"></use>
                    </g>
                    <g class="outer-ballun exit-ballun" transform="translate(-84.63423,212.99147)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#exit"></use>
                    </g>
                    <g class="outer-ballun escalator-ballun" transform="translate(-187.87923,-27.87153)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#escalator"></use>
                    </g>
                    <g class="outer-ballun escalator-ballun" transform="translate(75.425006,109.82821)">
                        <use xlink:href="#ballun"></use>
                        <use xlink:href="#escalator"></use>
                    </g>
                </svg>
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/lvl-0.png" alt="">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:news",
                    "rent",
                    Array(
                        "ADD_ELEMENT_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "AJAX_MODE" => "Y",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "BROWSER_TITLE" => "-",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "COMPONENT_TEMPLATE" => "rent",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                        "DETAIL_DISPLAY_TOP_PAGER" => "N",
                        "DETAIL_FIELD_CODE" => array(0=>"",1=>"",),
                        "DETAIL_PAGER_SHOW_ALL" => "Y",
                        "DETAIL_PAGER_TEMPLATE" => "",
                        "DETAIL_PAGER_TITLE" => "Страница",
                        "DETAIL_PROPERTY_CODE" => array(0=>"MARKET",1=>"CATEGORY",2=>"LOGO",3=>"",),
                        "DETAIL_SET_CANONICAL_URL" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FILTER_FIELD_CODE" => array(0=>"",1=>"",),
                        "FILTER_NAME" => "",
                        "FILTER_PROPERTY_CODE" => array(0=>"CATEGORY",1=>"",),
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => "9",
                        "IBLOCK_TYPE" => "rent",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "LIST_FIELD_CODE" => array(0=>"",1=>"",),
                        "LIST_PROPERTY_CODE" => array(0=>"MARKET",1=>"CATEGORY",2=>"LOGO",3=>"",),
                        "MESSAGE_404" => "",
                        "META_DESCRIPTION" => "-",
                        "META_KEYWORDS" => "-",
                        "NEWS_COUNT" => "100",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "SEF_MODE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "Y",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N",
                        "USE_CATEGORIES" => "N",
                        "USE_FILTER" => "N",
                        "USE_PERMISSIONS" => "N",
                        "USE_RATING" => "N",
                        "USE_RSS" => "N",
                        "USE_SEARCH" => "N",
                        "USE_SHARE" => "N",
                        "VARIABLE_ALIASES" => array("SECTION_ID"=>"/sheme/","ELEMENT_ID"=>"ELEMENT_ID",)
                        )
                );?>
                <?$APPLICATION->IncludeComponent("bitrix:news", "filter-0", Array(
                    "ADD_ELEMENT_CHAIN" => "N", // Включать название элемента в цепочку навигации
                        "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                        "AJAX_MODE" => "Y", // Включить режим AJAX
                        "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
                        "AJAX_OPTION_HISTORY" => "N",   // Включить эмуляцию навигации браузера
                        "AJAX_OPTION_JUMP" => "N",  // Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
                        "BROWSER_TITLE" => "-", // Установить заголовок окна браузера из свойства
                        "CACHE_FILTER" => "N",  // Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",  // Учитывать права доступа
                        "CACHE_TIME" => "36000000", // Время кеширования (сек.)
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CHECK_DATES" => "Y",   // Показывать только активные на данный момент элементы
                        "COMPOSITE_FRAME_MODE" => "A",  // Голосование шаблона компонента по умолчанию
                        "COMPOSITE_FRAME_TYPE" => "AUTO",   // Содержимое компонента
                        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
                        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",   // Выводить под списком
                        "DETAIL_DISPLAY_TOP_PAGER" => "N",  // Выводить над списком
                        "DETAIL_FIELD_CODE" => array(   // Поля
                            0 => "",
                            1 => "",
                        ),
                        "DETAIL_PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
                        "DETAIL_PAGER_TEMPLATE" => "",  // Название шаблона
                        "DETAIL_PAGER_TITLE" => "Страница", // Название категорий
                        "DETAIL_PROPERTY_CODE" => array(    // Свойства
                            0 => "CATEGORY",
                            1 => "MARKET",
                            2 => "LOGO",
                            3 => "",
                        ),
                        "DETAIL_SET_CANONICAL_URL" => "N",  // Устанавливать канонический URL
                        "DISPLAY_BOTTOM_PAGER" => "Y",  // Выводить под списком
                        "DISPLAY_DATE" => "Y",  // Выводить дату элемента
                        "DISPLAY_NAME" => "Y",  // Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",   // Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",  // Выводить текст анонса
                        "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
                        "FILTER_FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "FILTER_NAME" => "",
                        "FILTER_PROPERTY_CODE" => array(
                            0 => "CATEGORY",
                            1 => "",
                        ),
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",  // Скрывать ссылку, если нет детального описания
                        "IBLOCK_ID" => "9", // Инфоблок
                        "IBLOCK_TYPE" => "rent",   // Тип инфоблока
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
                        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",   // Формат показа даты
                        "LIST_FIELD_CODE" => array( // Поля
                            0 => "",
                            1 => "",
                        ),
                        "LIST_PROPERTY_CODE" => array(  // Свойства
                            0 => "CATEGORY",
                            1 => "MARKET",
                            2 => "LOGO",
                            3 => "",
                        ),
                        "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                        "META_DESCRIPTION" => "-",  // Установить описание страницы из свойства
                        "META_KEYWORDS" => "-", // Установить ключевые слова страницы из свойства
                        "NEWS_COUNT" => "100",  // Количество новостей на странице
                        "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                        "PAGER_DESC_NUMBERING" => "N",  // Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",   // Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                        "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
                        "PAGER_TEMPLATE" => ".default", // Шаблон постраничной навигации
                        "PAGER_TITLE" => "Новости", // Название категорий
                        "PREVIEW_TRUNCATE_LEN" => "",   // Максимальная длина анонса для вывода (только для типа текст)
                        "SEF_MODE" => "N",  // Включить поддержку ЧПУ
                        "SET_LAST_MODIFIED" => "N", // Устанавливать в заголовках ответа время модификации страницы
                        "SET_STATUS_404" => "N",    // Устанавливать статус 404
                        "SET_TITLE" => "Y", // Устанавливать заголовок страницы
                        "SHOW_404" => "N",  // Показ специальной страницы
                        "SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
                        "SORT_BY2" => "SORT",   // Поле для второй сортировки новостей
                        "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                        "SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
                        "STRICT_SECTION_CHECK" => "N",  // Строгая проверка раздела
                        "USE_CATEGORIES" => "N",    // Выводить материалы по теме
                        "USE_FILTER" => "N",    // Показывать фильтр
                        "USE_PERMISSIONS" => "N",   // Использовать дополнительное ограничение доступа
                        "USE_RATING" => "N",    // Разрешить голосование
                        "USE_RSS" => "N",   // Разрешить RSS
                        "USE_SEARCH" => "N",    // Разрешить поиск
                        "USE_SHARE" => "N", // Отображать панель соц. закладок
                        "COMPONENT_TEMPLATE" => ".default",
                        "VARIABLE_ALIASES" => array(
                            "SECTION_ID" => "SECTION_ID",
                            "ELEMENT_ID" => "ELEMENT_ID",
                        )
                    ),
                    false
                );?>
            </div>
        </div>
    </div>
</div> 
<div class="result">
</div>
<script>
    $(function(){
        $('.main-level path').mousemove(function(e){
                target  = $(this).attr('data-area-target');
                result  = $('.'+target).html();
            $(this).css({
                'opacity'   : '.3',
            })
            $('.result').html(result)
            $('.result').css({
                "top": (e.pageY + 16) + "px",
                "left": (e.pageX + 16) + "px"

            });
        })
        $('.main-level path').mouseleave(function(){
            $(this).css({
                'opacity'   : '0',
            })
            $('.result').html('');
        })
        $('.main-level path').click(function(){
                target  = $(this).attr('data-area-target');
                result  = $('.'+target).html();
                $detail = $('.'+target).find('a span');
                if($detail.html() == undefined){
                    alert
                }
                else{
                    $detail.trigger('click');
                    $('.overlay').fadeIn(500);
                    setTimeout(function(){
                        var resultPopup = $('.area-detail').html();
                        $('.result-popup').fadeIn(500).html(resultPopup);
                    },500);
                }
        });
        $(function(){
            floor = Cookies.get('floor');
            elementh = Cookies.get('elementh');
            if(floor !==undefined){
                $('.floor').removeClass('active');
                $('#'+floor).find('path').trigger('click');
                console.log('id этажа: '+floor,'; лежит в include/map.php');
                if(elementh !==undefined){
                    $('[data-area-target='+elementh+']').css({
                        'opacity'   : '.7',
                        'fill'      : 'red',
                    });
                }
            }
        });
    }); 
</script> 
<style>
    path,
    g{
        cursor: auto !important;
        fill: #333;
        opacity: .5 !important;
    }
    path.js-rent,
    g.js-rent{
        opacity: 0 !important;
        cursor: pointer !important;
    }
</style>