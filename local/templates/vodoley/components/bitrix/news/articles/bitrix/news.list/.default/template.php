<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="action container">
	<div class="action-container">
		<?if($arParams["DISPLAY_TOP_PAGER"]):?>
			<?=$arResult["NAV_STRING"]?><br />
		<?endif;?>
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			



                <div class="item" style="background-image: url(<?=$arItem['PREVIEW_PICTURE']['SRC']?>);">
					<?if(!empty($arItem['PROPERTIES']['DISCOUNT']['VALUE'])) {?>
						<div class="sale">
							<span>
								<?=$arItem['PROPERTIES']['DISCOUNT']['VALUE']?>
						 	</span>
						 </div>
					<?}?>
					<?if(!empty($arItem['PROPERTIES']['ACTIONS']['VALUE'])) {?>
						<div class="action-indicator">
							<span>
								<?=$arItem['PROPERTIES']['ACTIONS']['VALUE']?>
							</span>
						</div>
					<?}?>


                    


                    <h3 class="pink"><?=$arItem['NAME']?></h3>
                    <p><?=$arItem['~PREVIEW_TEXT']?></p>
                    <div class="button-row"><a class="button acsent-btn" href="<?=$arItem['~DETAIL_PAGE_URL']?>">подробнее</a>
					<?/*if(!empty($arItem['PROPERTIES']['SHEME']['VALUE'])){?>
                    	<a class="button default-btn" href="/">на схеме</a>
	
                    	<?
							if(!empty($arItem['PROPERTIES']['SHEME']['VALUE'])){
								$new_id = $arItem['PROPERTIES']['SHEME']['VALUE'];
								$APPLICATION->IncludeComponent(
									"bitrix:news.detail", 
									"actions-more_list", 
									array(
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"ADD_ELEMENT_CHAIN" => "N",
										"ADD_SECTIONS_CHAIN" => "Y",
										"AJAX_MODE" => "Y",
										"AJAX_OPTION_ADDITIONAL" => "",
										"AJAX_OPTION_HISTORY" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"BROWSER_TITLE" => "-",
										"CACHE_GROUPS" => "Y",
										"CACHE_TIME" => "36000000",
										"CACHE_TYPE" => "A",
										"CHECK_DATES" => "Y",
										"COMPOSITE_FRAME_MODE" => "A",
										"COMPOSITE_FRAME_TYPE" => "AUTO",
										"DETAIL_URL" => "",
										"DISPLAY_BOTTOM_PAGER" => "Y",
										"DISPLAY_DATE" => "Y",
										"DISPLAY_NAME" => "Y",
										"DISPLAY_PICTURE" => "Y",
										"DISPLAY_PREVIEW_TEXT" => "Y",
										"DISPLAY_TOP_PAGER" => "N",
										"ELEMENT_CODE" => "",
										"ELEMENT_ID" => "$new_id",
										"FIELD_CODE" => array(
											0 => "NAME",
											1 => "DETAIL_TEXT",
											2 => "DETAIL_PICTURE",
											3 => "",
										),
										//"IBLOCK_ID" => "7",
										"IBLOCK_TYPE" => "Areas",
										"IBLOCK_URL" => "",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
										"MESSAGE_404" => "",
										"META_DESCRIPTION" => "-",
										"META_KEYWORDS" => "-",
										"PAGER_BASE_LINK_ENABLE" => "N",
										"PAGER_SHOW_ALL" => "N",
										"PAGER_TEMPLATE" => ".default",
										"PAGER_TITLE" => "Страница",
										"PROPERTY_CODE" => array(
											0 => "MARKET",
											1 => "CONTACTS",
											2 => "",
										),
										"SET_BROWSER_TITLE" => "Y",
										"SET_CANONICAL_URL" => "N",
										"SET_LAST_MODIFIED" => "N",
										"SET_META_DESCRIPTION" => "Y",
										"SET_META_KEYWORDS" => "Y",
										"SET_STATUS_404" => "N",
										"SET_TITLE" => "N",
										"SHOW_404" => "N",
										"STRICT_SECTION_CHECK" => "N",
										"USE_PERMISSIONS" => "N",
										"USE_SHARE" => "N",
										"COMPONENT_TEMPLATE" => ".default"
									),
									false 
							);
							}
					}*/?>

                    </div>
                </div>


		<?endforeach;?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<br /><?=$arResult["NAV_STRING"]?>
		<?endif;?>
	</div>
</div>
