<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<script>
    $(function(){
        Cookies.remove('floor');
        Cookies.remove('elementh');
    })
</script>
<?
	// сортировку берем из параметров компонента
	$arSort = array(
	      $arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
	      $arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	   );
	// выбрать нужно id элемента, его имя и ссылку. Можно добавить любые другие поля, например PREVIEW_PICTURE или PREVIEW_TEXT
	$arSelect = array(
	      "ID",
	      "NAME",
	      "DETAIL_PAGE_URL"
	   );
	// выбираем активные элементы из нужного инфоблока. Раскомментировав строку можно ограничить секцией
	$arFilter = array (
	      "IBLOCK_ID" => $arResult["IBLOCK_ID"],
	      //"SECTION_CODE" => $arParams["SECTION_CODE"],
	      "ACTIVE" => "Y",
	      "CHECK_PERMISSIONS" => "Y",
	   );
	// выбирать будем по 1 соседу с каждой стороны от текущего
	$arNavParams = array(
	      "nPageSize" => 1,
	      "nElementID" => $arResult["ID"],
	   );
	$arItems = Array();
	$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams, $arSelect);
	$rsElement->SetUrlTemplates($arParams["DETAIL_URL"]);
	while($obElement = $rsElement->GetNextElement())
	      $arItems[] = $obElement->GetFields();
	// возвращается от 1го до 3х элементов в зависимости от наличия соседей, обрабатываем эту ситуацию      
	if(count($arItems)==3):
	   $arResult["TORIGHT"] = Array("NAME"=>$arItems[0]["NAME"], "URL"=>$arItems[0]["DETAIL_PAGE_URL"]);
	   $arResult["TOLEFT"] = Array("NAME"=>$arItems[2]["NAME"], "URL"=>$arItems[2]["DETAIL_PAGE_URL"]);
	elseif(count($arItems)==2):
	   if($arItems[0]["ID"]!=$arResult["ID"])
	      $arResult["TORIGHT"] = Array("NAME"=>$arItems[0]["NAME"], "URL"=>$arItems[0]["DETAIL_PAGE_URL"]);
	   else
	      $arResult["TOLEFT"] = Array("NAME"=>$arItems[1]["NAME"], "URL"=>$arItems[1]["DETAIL_PAGE_URL"]);
	endif;
	// в $arResult["TORIGHT"] и $arResult["TOLEFT"] лежат массивы с информацией о соседних элементах
?>


<section class="single-action-page container">
    <h2 class="zigzag"><?=$arResult['NAME']?></h2>
    <div class="single-action-page_body">
        <div class="img-wrap"><span class="image-title"><?=$arResult['NAME']?></span><!--TODO set if-->
            <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult['DETAIL_PICTURE']['ALT']?>">
        </div>
        <?=$arResult['~DETAIL_TEXT']?>
        <div class="btn-row">
            <div class="nav-btn">
				<?if(is_array($arResult["TOLEFT"])):?> 
				   <a class="prev-page" href="<?=$arResult["TOLEFT"]["URL"]?>"></a> 
				<?endif?>
				<?if(is_array($arResult["TORIGHT"])):?> 
				   <a class="next-page" href="<?=$arResult["TORIGHT"]["URL"]?>"></a> 
				<?endif?>
            </div><a class="button button__social">Поделиться</a>
			<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
			<script src="//yastatic.net/share2/share.js"></script>
			<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus,twitter,tumblr,viber,whatsapp,telegram"></div>
        </div>
    </div>


<?
	if(!empty($arResult['PROPERTIES']['SHEME']['VALUE'])){
		$new_id = $arResult['PROPERTIES']['SHEME']['VALUE'];
		$APPLICATION->IncludeComponent(
			"bitrix:news.detail", 
			"actions-more", 
			array(
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"ADD_ELEMENT_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "Y",
				"AJAX_MODE" => "Y",
				"AJAX_OPTION_ADDITIONAL" => "",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"BROWSER_TITLE" => "-",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"CHECK_DATES" => "Y",
				"COMPOSITE_FRAME_MODE" => "A",
				"COMPOSITE_FRAME_TYPE" => "AUTO",
				"DETAIL_URL" => "",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"ELEMENT_CODE" => "",
				"ELEMENT_ID" => "$new_id",
				"FIELD_CODE" => array(
					0 => "NAME",
					1 => "DETAIL_TEXT",
					2 => "DETAIL_PICTURE",
					3 => "",
				),
				//"IBLOCK_ID" => "7",
				"IBLOCK_TYPE" => "Areas",
				"IBLOCK_URL" => "",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
				"MESSAGE_404" => "",
				"META_DESCRIPTION" => "-",
				"META_KEYWORDS" => "-",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"PAGER_SHOW_ALL" => "N",
				"PAGER_TEMPLATE" => ".default",
				"PAGER_TITLE" => "Страница",
				"PROPERTY_CODE" => array(
					0 => "MARKET",
					1 => "CONTACTS",
					2 => "",
				),
				"SET_BROWSER_TITLE" => "Y",
				"SET_CANONICAL_URL" => "N",
				"SET_LAST_MODIFIED" => "N",
				"SET_META_DESCRIPTION" => "Y",
				"SET_META_KEYWORDS" => "Y",
				"SET_STATUS_404" => "N",
				"SET_TITLE" => "N",
				"SHOW_404" => "N",
				"STRICT_SECTION_CHECK" => "N",
				"USE_PERMISSIONS" => "N",
				"USE_SHARE" => "N",
				"COMPONENT_TEMPLATE" => ".default"
			),
			false 
	);
	}
?>

</section>