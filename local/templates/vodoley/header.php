<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="telephone=no" name="format-detection">
    <meta name="HandheldFriendly" content="true">

	<?$APPLICATION->ShowHead()?>
	<title><?$APPLICATION->ShowTitle()?></title>
	<!--[if (gt IE 9)|!(IE)]><!-->
    	<link href="<?=SITE_TEMPLATE_PATH?>/assets/css/main5km5d8ic3rj4b5bl0udi.min.css" rel="stylesheet" type="text/css">
    <!--<![endif]-->
   	<link href="<?=SITE_TEMPLATE_PATH?>/template_styles.css" rel="stylesheet" type="text/css">
    <meta property="og:title" content="ГАЛЕРЕЯ ВОДОЛЕЙ">
    <meta property="og:title" content="">
    <meta property="og:url" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="500">
    <meta property="og:image:height" content="300">
    <meta property="twitter:description" content="">
    <link rel="image_src" href="">
    <link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/favicon.png">
    <link rel="apple-touch-icon" href="<?=SITE_TEMPLATE_PATH?>/apple-touch-favicon.png">
    <link href="<?=SITE_TEMPLATE_PATH?>/favicon.svg" rel="icon" type="image/svg+xml">
    <script>
        (function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCL8sTtOK9nq1cvTsT9h6BArrpeP5go31Y&amp;callback=initMap" async defer></script>

    <script src="<?=SITE_TEMPLATE_PATH?>/distr/js/jquery.min.js"></script>
</head>
<body>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
    <header>
        <div class="header_top container">
            <div class="metro">
			    <?$APPLICATION->IncludeComponent(
			        "bitrix:main.include",
			        "",
			        Array(
			            "AREA_FILE_SHOW" => "file",
			            "PATH" => SITE_DIR."local/include/metro.php"
			        )
			    );?>
            </div>
            <div class="header_top__separ"></div>
            <div class="adress">
			    <?$APPLICATION->IncludeComponent(
			        "bitrix:main.include",
			        "",
			        Array(
			            "AREA_FILE_SHOW" => "file",
			            "PATH" => SITE_DIR."local/include/adress.php"
			        )
			    );?>
            </div>
            <div class="header_top__separ"></div>
            <div class="time-work">
			    <?$APPLICATION->IncludeComponent(
			        "bitrix:main.include",
			        "",
			        Array(
			            "AREA_FILE_SHOW" => "file",
			            "PATH" => SITE_DIR."local/include/time.php"
			        )
			    );?>
            </div>
            <div class="header_top__separ"></div>
            <div class="tel">
			    <?$APPLICATION->IncludeComponent(
			        "bitrix:main.include",
			        "",
			        Array(
			            "AREA_FILE_SHOW" => "file",
			            "PATH" => SITE_DIR."local/include/phone.php"
			        )
			    );?>
            </div>
            <div class="header_top__separ"></div>
            <div class="search">
                <a href="javascript:;"></a>
            </div>
        </div>
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"top",
				Array(
					"ALLOW_MULTI_SELECT" => "N",
					"CHILD_MENU_TYPE" => "left",
					"DELAY" => "N",
					"MAX_LEVEL" => "1",
					"MENU_CACHE_GET_VARS" => array(0=>"",),
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_TYPE" => "N",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"ROOT_MENU_TYPE" => "left",
					"USE_EXT" => "N"
				)
			);?>
        </div>
    </header>
