<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<section class="partners container">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide wrap-img">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/part-1.png" alt="">
            </div>
            <div class="swiper-slide wrap-img">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/part-2.png" alt="">
            </div>
            <div class="swiper-slide wrap-img">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/part-3.png" alt="">
            </div>
            <div class="swiper-slide wrap-img">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/part-4.png" alt="">
            </div>
            <div class="swiper-slide wrap-img">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/general/part-5.png" alt="">
            </div>
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</section>