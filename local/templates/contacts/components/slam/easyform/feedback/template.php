<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Localization\Loc;

$FORM_ID           = trim($arParams['FORM_ID']);
$FORM_AUTOCOMPLETE = $arParams['FORM_AUTOCOMPLETE'] ? 'on' : 'off';
$FORM_ACTION_URI   = "";
$WITH_FORM = strlen($arParams['WIDTH_FORM']) > 0 ? 'style="max-width:'.$arParams['WIDTH_FORM'].'"' : '';
?>
<div class="container ligth-bg">
	<div class="small-container slam-easyform<?=$arParams['HIDE_FORMVALIDATION_TEXT'] == 'Y' ? ' hide-formvalidation' : ''?>" <?=$WITH_FORM?>>
        <h2>Обратная связь</h2>
        <p>Заполните форму и в течении рабочего дня с Вами свяжется наш менеджер</p>
		<form id="<?=$FORM_ID?>" enctype="multipart/form-data" class="contact-page_form" method="POST" action="<?=$FORM_ACTION_URI;?>" autocomplete="<?=$FORM_AUTOCOMPLETE?>" novalidate="novalidate">

			<input type="hidden" name="FORM_ID" value="<?=$FORM_ID?>">
			<input type="text" name="ANTIBOT[NAME]" value="<?=$arResult['ANTIBOT']['NAME'];?>" class="hidden">

			<?//hidden fields
			foreach($arResult['FORM_FIELDS'] as $fieldCode => $arField)
			{
				if($arField['TYPE'] == 'hidden')
				{
					?>
					<input type="hidden" name="<?=$arField['NAME']?>" value="<?=$arField['VALUE'];?>"/>
					<?
					unset($arResult['FORM_FIELDS'][$fieldCode]);
				}
			}
			?>
			<div class="col">
				<?
				if(!empty($arResult['FORM_FIELDS'])):
					foreach($arResult['FORM_FIELDS'] as $fieldCode => $arField):

						if(!$arParams['HIDE_ASTERISK'] && !$arParams['HIDE_FIELD_NAME']){
							$asteriks = ':';
							if($arField['REQUIRED']) {
								$asteriks = '<span class="asterisk">*</span>:';
							}
							$arField['TITLE'] = $arField['TITLE'].$asteriks;
						}

						if($arField['TYPE'] == 'textarea'):?>
							<div class="col-xs-12">
								<div class="form-group">
									<? if(!$arParams['HIDE_FIELD_NAME']): ?>
									<? endif; ?>
									<div>
										<textarea class="form-control" id="<?=$arField['ID']?>" rows="5" name="<?=$arField['NAME']?>" <?=$arField['PLACEHOLDER_STR'];?> <?=$arField['REQ_STR']?>><?=$arField['VALUE'];?></textarea>
									</div>
								</div>
							</div>
						<?else:?>
							<? if(!$arParams['HIDE_FIELD_NAME']): ?>
							<? endif; ?>
							<input class="form-control" type="<?=$arField['TYPE'];?>" id="<?=$arField['ID']?>" name="<?=$arField['NAME']?>" value="<?=$arField['VALUE'];?>" <?=$arField['PLACEHOLDER_STR'];?> <?=$arField['REQ_STR']?> <?=$arField['MASK_STR']?>>
						<?endif;
					endforeach;?>
					<?if($arParams["USE_CAPTCHA"]):?>
						<div class="col-xs-12">
							<div class="form-group">
								<? if(!$arParams['HIDE_FIELD_NAME']): ?>
									<label for="<?=$FORM_ID?>-captchaValidator" class="control-label"><?=htmlspecialcharsBack($arParams['CAPTCHA_TITLE'])?></label>
								<? endif; ?>
								<input id="<?=$FORM_ID?>-captchaValidator"  class="form-control" type="text" required data-bv-notempty-message="<?=$arParams['CAPTCHA_ERROR']?>" name="captchaValidator" style="border: none; height: 0; padding: 0; visibility: hidden;">
								<div id="<?=$FORM_ID?>-captchaContainer"></div>
							</div>
						</div>
					<?endif;?>

					<div class="col-xs-12">
						<button type="submit" class="button acsent-btn" data-default="<?=$arParams['FORM_SUBMIT_VALUE']?>"><?=$arParams['FORM_SUBMIT_VALUE']?></button>
					</div>
				<?endif;?>
			</div>
		</form>

		<?if($arParams['SHOW_MODAL'] == 'Y'):?>
			<div class="modal fade modal-add-holiday" id="frm-modal-<?=$FORM_ID?>"  role='dialog' aria-hidden='true'>
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header clearfix">
							<button type="button" data-dismiss="modal" aria-hidden="true" class="close">&#10006;</button>

							<? if($arParams['TITLE_SHOW_MODAL'] || $arParams['FORM_NAME']): ?>
								<div class="title"><?=$arParams['TITLE_SHOW_MODAL'] ? : $arParams['FORM_NAME']?></div>
							<? endif?>

						</div>
						<div class="modal-body">
							<p class="ok-text"><?=$arParams['OK_TEXT']?></p>
						</div>
					</div>
				</div>
			</div>
		<?endif;?>
	</div>
</div>
<script type="text/javascript">
	var easyForm = new JCEasyForm(<?echo CUtil::PhpToJSObject($arParams)?>);
</script>
