<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="area--detail" style="">
    <div class="popup-shop" id="clothing-area">
        <div class="popup_row">
        	<div class="flex__center">
				<?if(empty($arResult['PROPERTIES']['GALLERY']['~VALUE']['TEXT'])) {?>
					<img class="area-logo" src="<?=$arResult['DISPLAY_PROPERTIES']['LOGO']['FILE_VALUE']['SRC']?>">
				<?}?>
				<div class="popup_img">
						<?=$arResult['PROPERTIES']['GALLERY']['~VALUE']['TEXT']?>
            		</div>
            </div>
            <div class="popup_detalied">
                <h2><?=$arResult['PROPERTIES']['MARKET']['~VALUE']?></h2>
                <p><?=$arResult['PREVIEW_TEXT']?></p>
                <div class="detalied-row">
                    <div class="detalied_col-1">
                        <ul>
                            <li>Секция 1, этаж 1</li>
                            <li>Bershka</li>
                            <li>Adidas</li>
                            <li>Pull And Bear</li>
                            <li>Fishka</li>
                            <li>10:00-22:00</li>
                        </ul>
                    </div>
                    <div class="detalied_col-2"><?=$arResult['DETAIL_TEXT']?></div>
                </div>
                <div class="detalied-row">
                    <div class="detalied_col-1"><?=$arResult['PROPERTIES']['CONTACTS']['~VALUE']['TEXT']?></div>
                    <div class="detalied_col-2 flex-container">
                    	<a class="button acsent-btn" href="javascript:;" data-area="<?=$arResult['NAME']?>">на схеме</a>
						<?if(!empty($arResult['PROPERTIES']['ACTIONS']['VALUE'])) {?>
                    		<a class="button button-outline button-black" href="/articles/?ELEMENT_ID=<?=$arResult['PROPERTIES']['ACTIONS']['VALUE']?>">об акции</a>
						<?}?>
                    </div>
                </div>
            </div>
        </div>
		<button class="btn btn__close"></button>
    </div>
</div>



	
<script>
	$(function(){
		$('.popup_img').slick({
		    arrows: false,
		    dots: true,
		    infinite: false,
		    slidesToShow: 1,
		    slidesToScroll: 1
		});
		$('.acsent-btn').click(function(){
			var area = $(this).data('data-area');
			console.log(area);
		});
	})
</script>
